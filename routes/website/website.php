<?php


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
| Prefix providers_management.
|
*/


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebsiteHomeController;
use App\Http\Controllers\InternalPageImageGroupController;
use App\Http\Controllers\InternalPageImageCommentController;
use App\Http\Controllers\InternalPageNewsController;
Route::resource('/website', WebsiteHomeController::class,
[
    'names' => [
        'index' => 'website.index', 
        
    ]
]);
Route::resource('/internal_page_image_group', InternalPageImageGroupController::class);
Route::resource('/internal_page_image_comment', InternalPageImageCommentController::class);
Route::resource('/internal_page_news', InternalPageNewsController::class);