<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'website'], function () {
    require_once __DIR__ . '/website/website.php';
});

Auth::routes();
Route::get('/', function () {
    return view('auth.login');
});


Route::middleware(['auth'])->group(function(){

Route::get('/deshboard', function () {
    return view('deshboard.index');
});
Route::get('/', function () {
    return view('deshboard.index');
});


Route::group(['prefix' => 'deshboard'], function () {
    require_once __DIR__ . '/deshboard/theme-managment-web.php';
});
Route::group(['prefix' => 'profile_management-web'], function () {
    require_once __DIR__ . '/deshboard/profile-management-web.php';
});
Route::get('style/{value}', [ProfileController::class, 'updateStyle']);

});