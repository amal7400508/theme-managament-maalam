<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FontController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\LogoController;
use App\Http\Controllers\SymbolController;
use App\Http\Controllers\ConnectInfoHeaderController;
use App\Http\Controllers\ConnectInfoFooterController;
use App\Http\Controllers\MainMenuController;
use App\Http\Controllers\SecMenuController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\StatController;
use App\Http\Controllers\ImageGroupController;
use App\Http\Controllers\ImageCommentController;
use App\Http\Controllers\LayoutsController;
use App\Http\Controllers\OrderMainMenuController;
use App\Http\Controllers\OrderSecMenuController;
use App\Http\Controllers\InternalPageController;
use App\Http\Controllers\SectionNameController; 
use App\Http\Controllers\ConnectInfoFooterOrderController; 
use App\Http\Controllers\ConnectInfoHeaderOrderController; 
use App\Http\Controllers\PartnersController; 
use App\Http\Controllers\FooterController; 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('deshboard')->middleware(['auth'])->group(function(){



  Route::resource('/font_managment', FontController::class,
  [
      'names' => [
          'index' => 'font_managment.index', 
          'store' => 'font_managment.store', 
          
      ]
  ]);

  Route::resource('/logo_managment', LogoController::class,
  [
      'names' => [
          'index' => 'logo_managment.index', 
          'store' => 'logo_managment.store', 
          
      ]
  ]);
  Route::resource('/color_managment', ColorController::class,
  [
      'names' => [
          'index' => 'color_managment.index', 
          'store' => 'color_managment.store', 
          
      ]
  ]);
  Route::resource('/symbols_managment', SymbolController::class,
  [
      'names' => [
          'index' => 'symbols_managment.index', 
          'store' => 'symbols_managment.store', 
          
      ]
  ]);
//header
//order
  Route::resource('/order_connect_header_managment', ConnectInfoHeaderOrderController::class,
  [
      'names' => [
          'index' => 'order_connect_header_managment.index', 
          'store' => 'order_connect_header_managment.store', 
         
      ]
  ]);
Route::post('/order', [ConnectInfoHeaderOrderController::class, 'order'])
->name('order_connect_header_managment.order');
//endorder
  Route::resource('/connect_header_managment', ConnectInfoHeaderController::class,
  [
      'names' => [
          'index' => 'connect_header_managment.index', 
          'store' => 'connect_header_managment.store', 
          
      ]
  ]);

//footer
//order
  Route::resource('/order_connect_footer_managment', ConnectInfoFooterOrderController::class,
  [
      'names' => [
          'index' => 'order_connect_footer_managment.index', 
          'store' => 'order_connect_footer_managment.store', 
         
      ]
  ]);
Route::post('/order', [ConnectInfoFooterOrderController::class, 'order'])
->name('order_connect_footer_managment.order');

//endorder
  Route::resource('/connect_footer_managment', ConnectInfoFooterController::class,
  [
      'names' => [
          'index' => 'connect_footer_managment.index', 
          'store' => 'connect_footer_managment.store', 
          
      ]
  ]); 





  Route::resource('/main_menu_managment', MainMenuController::class,
  [
      'names' => [
          'index' => 'main_menu_managment.index', 
          'store' => 'main_menu_managment.store', 
         
      ]
      ]);

      Route::resource('/section_name_managment', SectionNameController::class,
      [
          'names' => [
              'index' => 'section_name_managment.index', 
              'store' => 'section_name_managment.store', 
             
          ]
          ]);

  Route::resource('/order_main_menu_managment', OrderMainMenuController::class,
  [
      'names' => [
          'index' => 'order_main_menu_managment.index', 
          'store' => 'order_main_menu_managment.store', 
         
      ]
  ]);
Route::post('/order', [OrderMainMenuController::class, 'order'])->name('order_main_menu_managment.order');

Route::resource('/order_sec_menu_managment', OrderSecMenuController::class,
[
    'names' => [
        'index' => 'order_sec_menu_managment.index', 
        'store' => 'order_sec_menu_managment.store', 
       
    ]
]);
Route::post('/updateIdMenu', [OrderSecMenuController::class, 'updateIdMenu'])->name('order_sec_menu_managment.updateIdMenu');
 

Route::resource('/sec_menu_managment', SecMenuController::class,
  [
      'names' => [
          'index' => 'sec_menu_managment.index', 
          'store' => 'sec_menu_managment.store', 
          
      ]
  ]);


  Route::resource('/news_managment', NewsController::class,
  [
      'names' => [
          'index' => 'news_managment.index', 
          'store' => 'news_managment.store', 
          'edit' => 'news_managment.edit', 
          'update' => 'news_managment.update', 
          'destroy' => 'news_managment.update', 
          
      ]
  ]);

  Route::resource('/stat_managment', StatController::class,
  [
      'names' => [
          'index' => 'stat_managment.index', 
          'store' => 'stat_managment.store', 
         
      ]
  ]);
  Route::resource('/video_managment', VideoController::class,
  [
      'names' => [
          'index' => 'video_managment.index', 
          'store' => 'video_managment.store', 
          'edit' => 'video_managment.edit', 
          'update' => 'video_managment.update', 
          'destroy' => 'video_managment.update', 
          
      ]
  ]);

  Route::resource('/image_group_managment', ImageGroupController::class,
  [
      'names' => [
          'index' => 'image_group_managment.index', 
          'store' => 'image_group_managment.store', 
          
          
      ]
  ]);
  Route::post('/enable/{id}', [ImageGroupController::class, 'enable'])->name('image_group_managment.enable');
  Route::post('/disable/{id}', [ImageGroupController::class, 'disable'])->name('image_group_managment.disable');

  Route::resource('/image_comment_managment', ImageCommentController::class,
  [
      'names' => [
          'index' => 'image_comment_managment.index', 
          'store' => 'image_comment_managment.store', 
          
      ]
  ]);
  Route::post('/enablefun2/{id}', [ImageCommentController::class, 'enable'])->name('image_comment_managment.enable');
  Route::post('/disablefun2/{id}', [ImageCommentController::class, 'disable'])->name('image_comment_managment.disable');

  Route::resource('/partner_managment', PartnersController::class,
  [
      'names' => [
          'index' => 'partner_managment.index', 
          'store' => 'partner_managment.store', 
          
      ]
  ]);
  Route::post('/enablefun3/{id}', [PartnersController::class, 'enable']);
  Route::post('/disablefun3/{id}', [PartnersController::class, 'disable']);

  Route::resource('/layouts', LayoutsController::class,
  [
      'names' => [
          'index' => 'layouts.index', 
          'store' => 'layouts.store', 
          
      ]
  ]);
  Route::resource('/show_all_content', LayoutsController::class,
  [
      'names' => [
          'index' => 'show_all_content.index', 
          'store' => 'show_all_content.store', 
          
      ]
  ]);

  Route::resource('/footer_management', FooterController::class,
  [
      'names' => [
          'index' => 'footer_management.index', 
          'store' => 'footer_management.store', 
          
      ]
  ]);

});
