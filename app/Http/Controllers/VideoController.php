<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Models\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;

        $data = new Video();
      
        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();
        return view('deshboard.video_settings.index')
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $video = new Video();
       
        $video->tittle = $request->tittle;
        $video->desc_video = $request->desc_video;
        $video->link = $request->link;
        $video->save();

        $data = new Video();
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;
      
        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();
     
        return view('deshboard.video_settings.index')
         ->with('data', $data->paginate($table_length))
        ->with('data_count', $data_count)
        ->with('pagination_links', [
            'table_length' =>
                isset($_GET['table_length']) ? $_GET['table_length'] : '',
            'search_type' =>
                isset($_GET['search_type']) ? $_GET['search_type'] : '',
            'query' =>
                isset($_GET['query']) ? $_GET['query'] : '',
        ]
    )->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::find($id);
    
        return response()->json([
            'id' => $video->id,
            'titlle' => $video->tittle,
       
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {  $video = Video::find($id);
        $video->name = $request->name;
        $video->save();
        return response()->json([
            'status' => 200,
            'video' => $video,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        videoName::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => MainMenu::count()
        ],
            200
        );
    
    }
}
