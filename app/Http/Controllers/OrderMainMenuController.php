<?php

namespace App\Http\Controllers;
use App\Models\MainMenu;
use Illuminate\Http\Request;

class OrderMainMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $menuItems = MainMenu::orderBy('order', 'asc')->get();
     
  
       return view('deshboard.main_menu_settings.order', compact('menuItems'));
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
      

    }

    public function order(Request $request)
    {  $data = $request->input('order');
        foreach ($data as $index => $id) {
            MainMenu::where('id', $id)->update(['order' => $index]);
        }
        return response()->json([
            'status' => 200,
            'message' => 'تم تغيير ترتيب العنصر بنجاح.',
            //'message' => Lang::get('admin.added_successfully'),
            'alert-type' => 'success',
        ]);
    //return response()->json(['success' => $data]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id  
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
