<?php



namespace App\Http\Controllers;
use App\Models\ImageGroup;
use App\Models\Color;
use App\Models\News;
use App\Models\MainMenu;
use App\Models\SectionName;
use App\Models\Font;
use App\Models\SecMenu;
use App\Models\ImageComment;
use Illuminate\Http\Request;
use App\Models\ConnectInfoFooter;
use App\Models\ConnectInfoHeader;
use App\Models\Footer;
use Illuminate\Support\Facades\DB;

class InternalPageImageGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $color =new Color();
       
        $primaryColor = Color::where('name', 'primary-color')->pluck('color_code')->first();
        $SecColor = Color::where('name', 'secondary-color')->pluck('color_code')->first();
        $body_font = Font::where('name', 'body')->value('font_family');
        $title_font = Font::where('name', 'title')->value('font_family');
        $menu_font = Font::where('name', 'menu')->value('font_family');

        $css = ' :root {
            --primary-color: ' . $primaryColor . ';
            --secondary-color: ' . $SecColor . ';
        }';

        $css2= ' ul li, a {
            font-family: ' . $menu_font .'!important' .';
           
        }';
        
        $css3='body,div,p {
            font-family: ' . $body_font .'!important' .';
           
        }';

        $css4= 'h1,h2,h3,h4,h5,h6,span {
            font-family: ' . $title_font .'!important' .';
           
        }';

        $combinedCss=$css . $css2 .$css3 .$css4;
        
        $imageGroups = ImageGroup::get();
       
        $imageComments = ImageComment::get();

        $max_content_length = 200; // set your maximum content length here

        foreach ($imageComments as $imageComment) {
            if (strlen($imageComment->desc_image) > $max_content_length) {
                $imageComment->desc_short = substr($imageComment->desc_image, 0, $max_content_length) . '...';
                $imageComment->desc_long = substr($imageComment->desc_image, $max_content_length);
            } else {
                $imageComment->desc_short = $imageComment->desc_image;
                $imageComment->desc_long = null;
            }
        }

        file_put_contents(public_path('website_style/css/colors/scheme-01.css'), $combinedCss);
       
    
        


        $data = ImageGroup::findOrFail($id); 

        return view('deshboard.internal_pages.image_groups',['imageGroups' => $imageGroups,'data' => $data,'imageComments'=>$imageComments])
        ->with('news', News::where('page_id',1) ->where('status', 1)->get())
        // ->with('main_menus', MainMenu::orderBy('order', 'asc')get())
        ->with('sectionNames' , SectionName::with('imageGroups')->get())
        ->with('mainMenuItems' , MainMenu::orderBy('order')->with('secMenus')->get())
        ->with('ConnectInfoFooters' , ConnectInfoFooter::orderBy('order')->get())
        ->with('ConnectInfoHeaders' , ConnectInfoHeader::orderBy('order')->get())
        ->with('footers' , Footer::get());


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
