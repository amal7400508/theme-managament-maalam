<?php

namespace App\Http\Controllers;
use App\Models\SecMenu;
use App\Models\MainMenu;
use Illuminate\Http\Request;


class OrderSecMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    
    {  

     
        
         $mainMenuItems = MainMenu::orderby('order')->with('secMenus')->get();
    
    
         return view('deshboard.sec_menu_settings.order', compact('mainMenuItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = $request->input('order');

        foreach ($order as $index => $itemId) {
            $id = str_replace('item-', '', $itemId);
            $secondaryMenu = SecMenu::find($id);
            $secondaryMenu->order = $index;
            $secondaryMenu->save();
        }

        return response()->json([
            'status' => 200,
            'message' => 'تم تغيير ترتيب العنصر بنجاح.',
            //'message' => Lang::get('admin.added_successfully'),
            'alert-type' => 'success',
        ]);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }


    public function updateIdMenu(Request $request)

    {
        return'ppp';
          // Retrieve the item ID and new parent ID from the AJAX request
    $itemId = $request->input('item_id');
    $newParentId = $request->input('new_parent_id');
    
    // Update the menu item ID in the database
    $menuItem = SecMenu::find($itemId);
    $menuItem->main_menu_id = $newParentId;
    $menuItem->save();
    
    // Generate a new ID for the menu item based on its new parent
    $newId = 'menu-item-' . $newParentId . '-' . $itemId;
    
    // Create a response object with the new ID
    $response = [
        'new_id' => $newId
    ];
    
    // Return the response as JSON
    return response()->json($response);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
