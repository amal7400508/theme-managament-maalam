<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Models\MainMenu;
use Illuminate\Http\Request;

class MainMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {$table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;

        $data = new MainMenu();
      
        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();
        return view('deshboard.main_menu_settings.index')
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }
        

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    
    {   
        $menu = new MainMenu();
       
        $menu->name = $request->name;
        $menu->save();

        $data = new MainMenu();
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;
      
        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();
     
        return view('deshboard.main_menu_settings.index')
         ->with('data', $data->paginate($table_length))
        ->with('data_count', $data_count)
        ->with('pagination_links', [
            'table_length' =>
                isset($_GET['table_length']) ? $_GET['table_length'] : '',
            'search_type' =>
                isset($_GET['search_type']) ? $_GET['search_type'] : '',
            'query' =>
                isset($_GET['query']) ? $_GET['query'] : '',
        ]
    )->with('success', Lang::get('admin.added_successfully'));
       
      
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $main_menu = MainMenu::find($id);
    
        return response()->json([
            'id' => $main_menu->id,
            'name' => $main_menu->name,
       
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $main_menu = MainMenu::find($id);
        $main_menu->name = $request->name;
        $main_menu->save();
        return response()->json([
            'status' => 200,
            'main_menu' => $main_menu,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MainMenu::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => MainMenu::count()
        ],
            200
        );
    }
}
