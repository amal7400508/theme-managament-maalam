<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Models\ConnectInfoFooter;
use Illuminate\Http\Request;

class ConnectInfoFooterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
    $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

    if ($table_length == '') $table_length = 10;

    $data = new ConnectInfoFooter ();
  
    $data = $data->orderBy('id', 'desc');
    $data_count = $data->count();
    return view('deshboard.connect_footer_settings.index')
        ->with('data', $data->paginate($table_length))
        ->with('data_count', $data_count)
        ->with('pagination_links', [
                'table_length' =>
                    isset($_GET['table_length']) ? $_GET['table_length'] : '',
                'search_type' =>
                    isset($_GET['search_type']) ? $_GET['search_type'] : '',
                'query' =>
                    isset($_GET['query']) ? $_GET['query'] : '',
            ]
        );
    
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    
    {   
        $InfoFooter = new ConnectInfoFooter();
       
        $InfoFooter->icon = $request->icon;
        $InfoFooter->link = $request->link;
     
        $InfoFooter->save();

        $data = new ConnectInfoFooter();
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;
      
        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();
     
        return view('deshboard.connect_footer_settings.index')
         ->with('data', $data->paginate($table_length))
        ->with('data_count', $data_count)
        ->with('pagination_links', [
            'table_length' =>
                isset($_GET['table_length']) ? $_GET['table_length'] : '',
            'search_type' =>
                isset($_GET['search_type']) ? $_GET['search_type'] : '',
            'query' =>
                isset($_GET['query']) ? $_GET['query'] : '',
        ]
    )->with('success', Lang::get('admin.added_successfully'));
       
      
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $InfoFooter = ConnectInfoFooter::find($id);
    
        return response()->json([
            'id' => $InfoFooter->id,
            'link' => $InfoFooter->link,
            'icon' => $InfoFooter->icon,
       
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $InfoFooter = ConnectInfoFooter::find($id);
        $InfoFooter->icon = $request->icon;
        $InfoFooter->link = $request->link;
        $InfoFooter->save();
        return response()->json([
            'status' => 200,
            'InfoFooter' => $InfoFooter,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ConnectInfoFooter::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => MainInfoFooter::count()
        ],
            200
        );
    }
}
