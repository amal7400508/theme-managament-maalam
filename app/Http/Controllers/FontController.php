<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Font;
class FontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $body_font = Font::where('name', 'body')->value('font_family');
        $title_font = Font::where('name', 'title')->value('font_family');
        $menu_font = Font::where('name', 'menu')->value('font_family');

        return view('deshboard.font_settings.index', [
            'body_font' => $body_font,
            'title_font' => $title_font,
            'menu_font' => $menu_font,
            
        ]);
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $font_family_menu= $request->input('font_family_menu');
        $font_family_title = $request->input('font_family_title');
        $font_family_body = $request->input('font_family_body');
     
        $font_family_menu_db = Font::where('name', 'menu')->first();
        $font_family_menu_db->font_family = $font_family_menu;
        $font_family_menu_db->save();
        
        $font_family_title_db = Font::where('name', 'title')->first();
        $font_family_title_db->font_family = $font_family_title;
        $font_family_title_db->save();

        $font_family_body_db = Font::where('name', 'body')->first();
        $font_family_body_db->font_family = $font_family_body;
        $font_family_body_db->save();


        return view('deshboard.font_settings.index', [
            'body_font' => $font_family_body,
            'title_font' => $font_family_title,
            'menu_font' => $font_family_menu,
        ])->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
