<?php

namespace App\Http\Controllers;
use App\Models\Color;
use Illuminate\Http\Request;
use App\Utilities\Helpers\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $primary_color = Color::where('name', 'primary-color')->value('color_code');
        $secondary_color = Color::where('name', 'secondary-color')->value('color_code');

        return view('deshboard.color_settings.index', [
            'primary_color' => $primary_color,
            'secondary_color' => $secondary_color,
        ]);

        //return view('deshboard.color_settings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $primary_color = $request->input('main_color');
        $secondary_color = $request->input('secondary_color');
        
     
        $primary_color_db = Color::where('name', 'primary-color')->first();
        $primary_color_db->color_code = $primary_color;
        $primary_color_db->save();
        
        $secondary_color_db = Color::where('name', 'secondary-color')->first();
        $secondary_color_db->color_code = $secondary_color;
        $secondary_color_db->save();
        
      
        return view('deshboard.color_settings.index', [
            'primary_color' => $primary_color,
            'secondary_color' => $secondary_color,
        ])->with('success', Lang::get('admin.added_successfully'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $primary_color = $request->input('primary_color');
        $secondary_color = $request->input('secondary_color');
        
     
        $primary_color_db = Color::where('name', 'primary-color')->first();
        $primary_color_db->color_code = $primary_color;
        $primary_color_db->save();
        
        $secondary_color_db = Color::where('name', 'secondary-color')->first();
        $secondary_color_db->color_code = $secondary_color;
        $secondary_color_db->save();
        
      
        return redirect();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
