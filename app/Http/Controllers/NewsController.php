<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Request;
use App\Models\News;
use App\Models\Page;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;

        $data = new News();
      
        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();
        return view('deshboard.news_settings.index')
            ->with('data', $data->paginate($table_length))
            ->with('pages', Page::get())
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }
        
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news = new News();
       
        $news->name = $request->name;
        $news->page_id = $request->page;
        $news->status = $request->input('status',0);
        $news->save();

        $data = new News();
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;
      
        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();
     
        return view('deshboard.news_settings.index')
         ->with('data', $data->paginate($table_length))
        ->with('pages', Page::get())
        ->with('data_count', $data_count)
        ->with('pagination_links', [
            'table_length' =>
                isset($_GET['table_length']) ? $_GET['table_length'] : '',
            'search_type' =>
                isset($_GET['search_type']) ? $_GET['search_type'] : '',
            'query' =>
                isset($_GET['query']) ? $_GET['query'] : '',
        ]
    )
        ->with('success', Lang::get('admin.added_successfully'));
       
        // return redirect()->back()->with('success', Lang::get('admin.added_successfully'));
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
