<?php

namespace App\Http\Controllers;
use App\Models\ImageGroup;
use App\Models\ImageComment;
use App\Models\Partner;
use App\Models\SectionName;
use App\Utilities\Helpers\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class PartnersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {    
        
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;

   $data= SectionName::whereHas('partners')->get();
       $count1=Partner::select('section_name_id')->distinct()->get()->pluck('section_name_id')->count(); 
       $count2=ImageGroup::select('section_name_id')->distinct()->get()->pluck('section_name_id')->count();
       $count3=ImageComment::select('section_name_id')->distinct()->get()->pluck('section_name_id')->count();
      $all_conut=$count1+$count2+$count3;
       
       
        $data_count = $data->count();
        return view('deshboard.partner_settings.index')
        ->with('section_names',SectionName::get())
        ->with('all_conut',$all_conut)
        ->with('data',$data)
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
        

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required',
            'tittle' => 'required',
            'section_name' => 'required',
        ]);
        if ($request->image) {
        }
        if ($request->hasFile('image')) {
            $imageName = Image::reSizeImage($request->file('image'), 'storage/partner/', 'partner_');
        }
    


        $imageGroup = new Partner();
        $imageGroup->image_name = $imageName;
        $imageGroup->tittle = $request->input('tittle');
         $imageGroup->section_name_id = $request->input('section_name');
        $imageGroup->save();
        return view('deshboard.partner_settings.index')
        ->with('section_names',SectionName::get())
        ->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $section = SectionName::find($id);
        $data= SectionName::whereHas('partners')->get();
        $positions = $data->pluck('position')->toArray();
        $data_image_comments= SectionName::whereHas('imageComments')->get();
        $image_comments_section = $data_image_comments->pluck('id')->toArray();
        $data_image_group= SectionName::whereHas('imageGroups')->get();
        $image_groups_section = $data_image_group->pluck('id')->toArray();

        $count1=Partner::select('section_name_id')->distinct()->get()->pluck('section_name_id')->count(); 
        $count2=ImageGroup::select('section_name_id')->distinct()->get()->pluck('section_name_id')->count();
        $count3=ImageComment::select('section_name_id')->distinct()->get()->pluck('section_name_id')->count();
       $all_conut=$count1+$count2+$count3;
     
    
        return response()->json([
            'image_comments_section'=>$image_comments_section,
            'image_groups_section'=>$image_groups_section,
            'data'=>$data,
            'id' => $section->id,
            'name' => $section->name,
            'position' => $section->position,
            'positions' => $positions,
            'all_conut'=>$all_conut,
       
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {    
        $section = SectionName::find($id);
     
        $section->position = $request->section_position;
        $section->save();
        return response()->json([
            'status' => 200,
            'section' => $section,
            'partner'=>Partner::where ('section_name_id',$id)->first(),
            'title' => Lang::get('admin.postion_chnage_successfully'),
            'message' => Lang::get('admin.postion_chnage_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function enable(Request $request,$id)
    {
        $Partner = Partner::where ('section_name_id',$id)->update(['status' => 1]);

   

        return response ()->json ([
            'status' => 200,
            'data' => SectionName::find ($id),
            'title' => Lang::get ('admin.successfully_done'),
            'message' => ''
        ]);
    
    }

    public function disable(Request $request,$id)
    {   

        $Partner = Partner::where ('section_name_id',$id)->update(['status' => 0]);

     

        return response ()->json ([
            'status' => 200,
            'data' => SectionName::find ($id),
            'title' => Lang::get ('admin.successfully_done'),
            'message' => ''
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
