<?php


namespace App\Utilities\Helpers;


use Illuminate\Support\Facades\File;
use Image as ImageFaker;

class Image
{
    /**
     * Resize an images to different sizes and insert them into folders.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string                    $file_path
     * @return $imageName
     */
    public static function reSizeImage(
        $image,
        $file_path,
        $image_name
    )
    {
        $imageName = $image_name.time().'.'.$image->getClientOriginalExtension();

        // Check if folder is exists
        $path640 = public_path($file_path.'/640/'); // For details
        $path360 = public_path($file_path.'/360/'); // For normal
        $path150 = public_path($file_path.'/150/'); // For phone
        $path64  = public_path($file_path.'/64/'); // For search

        File::exists($path640) ?: File::makeDirectory($path640, $mode = 0777, true, true);
        File::exists($path360)
            ?: File::makeDirectory($path360, $mode = 0777, true, true);
        File::exists($path150)
            ?: File::makeDirectory($path150, $mode = 0777, true, true);
        File::exists($path64)
            ?: File::makeDirectory($path64, $mode = 0777, true, true);

        ImageFaker::make($image)
            ->resize(640, 640)
            ->fit(640, 640)->save($file_path.'/640/' . $imageName);
        ImageFaker::make($image)
            ->resize(360, 360)
            ->fit(360, 360)->save($file_path.'/360/' . $imageName);
        ImageFaker::make($image)
            ->resize(150, 150)
            ->fit(150, 150)->save($file_path.'/150/' . $imageName);
        ImageFaker::make($image)
            ->resize(64, 64)
            ->fit(64, 64)->save($file_path.'/64/' . $imageName);
        return $imageName;
    }

    /**
     * Deletes the image of current logged in user.
     *
     * @param $file_path        string the path of selected file to be deleted.
     * @param $image_name_store string the name of the file which is an image of user.
     *
     * @return array|string name of deleted items
     */
    public static function deleteImageFromFolder(
        $file_path, $image_name_store
    ){
        $deleted[] = [];
        $folders_name=['/640/','/360/','/150/','/64/'];
        for ($i=0;$i<count($folders_name);$i++){
            $filename= public_path().'/'
                .$file_path.$folders_name[$i]
                .$image_name_store;
            $deleted[] .= File::delete($filename);
        }
        return $deleted;
    }
}
