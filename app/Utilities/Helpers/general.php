<?php


use Illuminate\Support\Facades\File;

/**
 * @param $image
 * @param $path
 * @return string
 */
function uploadImage($image, $path)
{
    $imageName = $path . '_' . time() . '.' . $image->getClientOriginalExtension();
    $image->move(public_path('storage' . '/' . $path), $imageName);
    return $imageName;
}


/**
 * @param $file_name
 */
function deleteImage($file_name)
{
    $file_name = public_path() . $file_name;
    File::delete($file_name);
}

