<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    use HasFactory;
    protected $table = 'footers';
    protected $fillable = ['service', 'copyright'];



 

    protected $appends = [
        'actions',
        'background_color_row'
    ];


    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
      
            $actions .= '<a class="delete" id="' . $this->id . '" title="' . __('admin.delete') . '"><i class="ft-trash-2 color-red" style="margin: auto 8px"></i></a>';
        
        
            $actions .= '<a class="edit-table-row" id="' . $this->id . '" title="' . __ ('admin.edit') . '"><i class="ft-edit color-primary"></i></a>';

        


     


        return $actions;
    }

    public function getBackgroundColorRowAttribute()
    {
        return $this->status != 1 ? 'background-color: #ff041508;' : '';
    }
}
