<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use HasFactory;

  
    protected $table = 'partners';
    protected $fillable = [ 'image_name','tittle'];
    
    public function section_names()
    {
        return $this->belongsTo(SectionName::class, 'section_name_id');

    }
}
