<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageGroup extends Model
{
   
    use HasFactory;
    protected $table = 'image_group';
    protected $fillable = ['desc_image', 'image_name','tittle'];

    public function section_names()
    {
        return $this->belongsTo(SectionName::class, 'section_name_id');

    }

    protected $appends = [
        'actions',
        'background_color_row'
    ];

    
    /**
     * The buttons in datatable
     */
    public function getActionsAttribute($indexToShow = 3)
    {
        $PositionChange='';
    // $data = $this->select('image_group.section_name_id', 'section_name.name')
    //              ->join('section_name', 'image_group.section_name_id', '=', 'section_name.id')
    //              ->distinct('section_name_id')
    //              ->get();

                //  $data = ImageGroup::select('section_name_id')->distinct()->get();

                //  $PositionChange = '';
                //  foreach ($data as $image_group) {
                //      $class = 'position_change active' ;
                //      $PositionChange .= '<a class="' . $class . '" id="' . $image_group->section_names->id . '" title="' . __('admin.PositionChange') . '"><i class="btn btn-primary">تغيير الموقع</i></a>';
                //  }
             
                //  return $PositionChange;
                   // $actions = '';
        // $PositionChange='';
            // $actions .= '<a class="delete" id="' . $this->id . '" title="' . __('admin.delete') . '"><i class="ft-trash-2 color-red" style="margin: auto 8px"></i></a>';
        
        



            // $PositionChange .= '<a class="position_change btn btn-primary" id="' . $this->id . '" title="' . __ ('admin.PositionChange') . ' " style="font-size:x-small">PositionChange</a>';

            // foreach ($data as $image_group) {
            //     $PositionChange .= '<a class="position_change" data-id="' . $image_group->section_name_id . '" title="' . __('admin.PositionChange') . '">' . $image_group->section_name . '</a>';
     
            // }
         return 
           
            
            $PositionChange;
         
     }

    public function getBackgroundColorRowAttribute()
    {
        return $this->status != 1 ? 'background-color: #ff041508;' : '';
    }

}
