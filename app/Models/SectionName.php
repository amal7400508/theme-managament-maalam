<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SectionName extends Model
{
    use HasFactory;
    protected $table = 'section_name';
    protected $fillable = ['name'];

    public function imageGroups()
    {
        return $this->hasMany(ImageGroup::class);
    }

    public function partners()
    {
        return $this->hasMany(Partner::class);
    }

    
    public function imageComments()
    {  
    
        return  $this->hasMany(ImageComment::class);
    }
    protected $appends = [
        'actions',
        'actions2',
        'postions',
        'background_color_row'
    ];

    
    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
      
          $actions .=  '<button type="button" class="disable  btn" id="' . $this->id . '"
           style="background: #8f0b0b; color: white;padding: 8px;width:40%;">' . __('admin.disable') . '</button>' ;
           // $actions .=  '<button type="button" class="enable btn btn-sm btn-outline-success" id="' . $this->id . '">' . __('admin.enable') . '</button>' 
        ;
        

return $actions;

    }
    
    public function getActions2Attribute()
    {
        $actions2 = '';
      
       

  
    $actions2 .=  '<button type="button" class="enable btn" id="' . $this->id . '"
    style="background: #8f0b0b; color: white;padding: 8px;width:40%;">' . __('admin.enable') . '</button>' ;


return $actions2;

    }


    public function getPostionsAttribute()
    {
        $PositionChange = '';
      
           $PositionChange .= '<a class="position_change btn btn-primary" id="' . $this->id . '" title="' . __ ('admin.PositionChange') . ' " style="font-size:small"> تغيير الموقع </a>';
        
        
         

     


        return $PositionChange;
    }


    public function getBackgroundColorRowAttribute()
    {
        return $this->status != 1 ? 'background-color: #ff041508;' : '';
    }
}
