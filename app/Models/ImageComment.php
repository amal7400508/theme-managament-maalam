<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageComment extends Model
{
    use HasFactory;
    protected $table = 'image_comments';
    protected $fillable = ['desc_image', 'image_name','tittle'];
    
    public function section_names()
    {
        return $this->belongsTo(SectionName::class, 'section_name_id');

    }
}
