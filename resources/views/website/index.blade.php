<?php
    use App\Models\Logo;
    $logo = Logo::where('name','logo')->first(); // Retrieve the logo model instance
    $imageName = $logo->image_name; // Get the image name
?>

<!DOCTYPE html>
<html lang="ar">

<head>
    <meta charset="utf-8" />
    <title>جمعية خيرية</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <!-- <meta content="Priva - Insurance Company Website Template" name="description" /> -->
    <meta content="" name="keywords" />
    <meta content="" name="author" />
      <!-- Fonts -->
      
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
  
      
      <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet"/>
    
       <!-- Fonts -->
    <link rel="icon" type="image/png" sizes="36x36" href="{{ asset('storage/logo/64/'.$imageName) }}">
    <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
        <![endif]-->

    <!-- CSS Files
        
    ================================================== -->
    <!-- add Stylesheets -->
    <link id="bootstrap" href="{{ asset('website_style/css/bootstrap-rtl.min.css') }}" rel="stylesheet">
<link id="bootstrap-grid" href="{{ asset('website_style/css/bootstrap-grid-rtl.min.css') }}" rel="stylesheet">
<link id="bootstrap-reboot" href="{{ asset('website_style/css/bootstrap-reboot-rtl.min.css') }}" rel="stylesheet">
    <link href="{{asset('website_style/customized_style.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/font-awesome-all.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/flaticon.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/owl.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/jquery.fancybox.min.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/style2.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/responsive.css')}}" rel="stylesheet">


    <link href="{{asset('website_style/css/jpreloader.css')}}" rel="stylesheet" type="text/css">
    <link id="bootstrap" href="{{asset('website_style/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <link id="bootstrap-grid" href="{{asset('website_style/css/bootstrap-grid-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <link id="bootstrap-reboot" href="{{asset('website_style/css/bootstrap-reboot-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/animate.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/owl.theme.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/owl.transitions.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/jquery.countdown.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/style.css')}}" rel="stylesheet" type="text/css" />

    <!-- color scheme -->
    <link id="colors" href="{{asset('website_style/css/colors/scheme-01.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/coloring.css')}}" rel="stylesheet" type="text/css" />
    <link id="colors" href="{{asset('website_style/css/colors/scheme-01-v2.css')}}" rel="stylesheet" type="text/css" />

    <!-- RS5.0 Stylesheet -->
    <link rel="stylesheet" href="{{asset('website_style/revolution/css/settings.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('website_style/revolution/css/layers.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('website_style/revolution/css/navigation.css')}}" type="text/css">

</head>

<body class="hide-rtl">
    <div id="wrapper">
        <div id="topbar" class="text-light">
            <div class="container">
                <div class="topbar-left sm-hide">
                    <span class="topbar-widget tb-social">
                        @foreach ($ConnectInfoHeaders as $ConnectInfoHeader)
                        <a href="{{$ConnectInfoHeader->link}}"> 
                            <i class="fa {{$ConnectInfoHeader->icon}} -color-secondary"></i>
                        </a>
                        
                        @endforeach
                    </span>
                </div>
            
                <div class="clearfix"></div>
            </div>
        </div>

        <!-- header begin -->
        <header class="transparent">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="de-flex sm-pt10">
                            <div class="de-flex-col">
                                <!-- logo begin -->
                                <div id="logo">
                                    <a href="index.html">
                                        <img alt="" class="logo" src="{{ asset('storage/logo/64/'.$imageName) }}" width="80px"
                                            height="50px" style="margin-bottom: 30px;" />
                                        
                                    </a>
                                </div>
                                <!-- logo close -->
                            </div>
                            <div class="de-flex-col header-col-mid">
                                <!-- mainmenu begin -->
                                <ul id="mainmenu" style=" font-size: 18px;    margin-left: 75px;">
                                   
                                    @foreach ($mainMenuItems as $mainMenuItem)
                                   
                                    <li style="float: right;">
                                        <a href="index.html">{{ $mainMenuItem->name }}<span></span></a>
                                       
                                        <ul>
                                            @foreach ($mainMenuItem->secMenus as $secondaryMenuItem)
                                           
                                            <li style="float: right;">
                                            <a href="index.html" style="text-align: right !important;">
                                                {{$secondaryMenuItem->name}}</a> </li>
                                            @endforeach
                                      
                                        </ul>
                                    

                                      
                                    </li>
                                    @endforeach
                                  
                                    <li style="float: right;"></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header close -->
        <!-- content begin -->
        <div class="no-bottom no-top" id="content">
            <div id="top"></div>
          
            <!-- revolution slider begin -->
            
            <section id="section-slider" class=" fullwidthbanner-container text-light" aria-label="section-slider">
                <div id="slider-revolution">
                    <ul>
                        <li data-transition="fade" data-slotamount="10" data-masterspeed="300" data-thumb="">
                            <!--  BACKGROUND IMAGE -->
                            <img alt="" class="rev-slidebg" data-bgposition="top center" data-bgfit="cover"
                                data-bgrepeat="no-repeat" data-bgparallax="10" src="{{asset('website_style/images/slider/1.jpg')}}">
                            <div class=" mask" 
                            style="position: absolute;
                                width: 100%;
                                height: 120%;
                                top: -40px;
                                background-color: #161d2ccc;
                                 background-size: cover;
                                 clip-path: polygon(0 10%, 98% 0, 70% 40%, 64% 54%, 50% 70%, 0 163%);">

                                 kmkmjkimjki
                            </div>

                            <div class=" tp-caption big-s1" data-x="0" data-y="230" data-width="none" data-height="none"
                                data-whitespace="nowrap" data-transform_in="y:100px;opacity:0;s:500;e:Power2.easeOut;"
                                data-transform_out="opacity:0;y:-100;s:200;e:Power2.easeInOut;" data-start="500"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on">


                            </div>
                            <div class=" tp-caption very-big-white tp-resizeme" data-x="0" data-y="280"
                                data-width="none" data-height="none" data-whitespace="nowrap"
                                data-transform_in="y:100px;opacity:0;s:500;e:Power2.easeOut;"
                                data-transform_out="opacity:0;y:-100;s:400;e:Power2.easeInOut;" data-start="600"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on">
                                <h3 style="margin-right: -18px;">العمل الجماعي</h3>
                                <h1> مع
                                    <span style="color:var(--secondary-color)"> الأمل والتفاني </span> يجعل الأحلام تتحقق </h1>
                            </div>


                            <div class="mt-5 tp-caption" data-x="0" data-y="360" data-width="480" data-height="none"
                                data-whitespace="wrap" data-transform_in="y:100px;opacity:0;s:500;e:Power2.easeOut;"
                                data-transform_out="opacity:0;y:-100;s:600;e:Power2.easeInOut;" data-start="700">
                                <p class="lead xs-hide" style="margin-right: -100px; width: 500px;color: #ededed;">
                                    نخلق فرصًا للتعلم وتنمية المهارات الشخصية والاجتماعية لأفراد المجتمع خاصة الفئات
                                    المحرومة. ونعزز ثقافة العطاء والتسامح وتشجع على تحقيق العدالة الاجتماعية</p>
                            </div>
                            <div class="tp-caption" data-x="0" data-y="450" data-width="none" data-height="none"
                                data-whitespace="nowrap" data-transform_in="y:100px;opacity:0;s:500;e:Power2.easeOut;"
                                data-transform_out="opacity:0;y:-100;s:800;e:Power2.easeInOut;" data-start="800">

                            </div>
                        </li>
                        <li data-transition="fade" data-slotamount="10" data-masterspeed="300" data-thumb="">

                            <img alt="" class="rev-slidebg" data-bgposition="top center" data-bgfit="cover"
                                data-bgrepeat="no-repeat" data-bgparallax="10" src="{{asset('website_style/images/slider/2.jpg')}}">
                            <div class="mask " style="position: absolute;width:100%;
                                height: 120%;
                                top: -40px;
                                background-color: #161d2ccc;
                                 background-size: cover;
                                 clip-path: polygon(0 10%, 98% 0, 70% 40%, 64% 54%, 50% 70%, 0 163%);
                          ">
                            </div>

                            <div class=" tp-caption big-s1" data-x="0" data-y="230" data-width="none" data-height="none"
                                data-whitespace="nowrap" data-transform_in="y:100px;opacity:0;s:500;e:Power2.easeOut;"
                                data-transform_out="opacity:0;y:-100;s:200;e:Power2.easeInOut;" data-start="500"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on">

                            </div>
                            <div class=" tp-caption very-big-white tp-resizeme" data-x="0" data-y="280"
                                data-width="none" data-height="none" data-whitespace="nowrap"
                                data-transform_in="y:100px;opacity:0;s:500;e:Power2.easeOut;"
                                data-transform_out="opacity:0;y:-100;s:400;e:Power2.easeInOut;" data-start="600"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on">
                                <h3 style="margin-right: -18px;">العمل الجماعي</h3>
                                <h1> مع
                                    <span style="color:var(--secondary-color)"> الأمل والتفاني </span> يجعل الأحلام تتحقق </h1>
                            </div>
                            <div class="mt-5 tp-caption" data-x="0" data-y="360" data-width="480" data-height="none"
                                data-whitespace="wrap" data-transform_in="y:100px;opacity:0;s:500;e:Power2.easeOut;"
                                data-transform_out="opacity:0;y:-100;s:600;e:Power2.easeInOut;" data-start="700">
                                <p class="lead xs-hide" style="margin-right: -100px; width: 500px;color: #ededed;">نخلق
                                    فرصًا للتعلم وتنمية المهارات الشخصية والاجتماعية لأفراد المجتمع خاصة الفئات
                                    المحرومة. ونعزز ثقافة العطاء والتسامح وتشجع على تحقيق العدالة الاجتماعية</p>
                            </div>
                            <div class="tp-caption" data-x="0" data-y="450" data-width="none" data-height="none"
                                data-whitespace="nowrap" data-transform_in="y:100px;opacity:0;s:500;e:Power2.easeOut;"
                                data-transform_out="opacity:0;y:-100;s:800;e:Power2.easeInOut;" data-start="800">

                            </div>
                        </li>


                    </ul>
                </div>

            </section>
            <!-- revolution slider close -->

            <section class="pt40 pb40 bg-color-secondary " style="background-color: var(--primary-color) !important;
            background-image: url({{asset('website_style/images/background/layer-image-7.png')}});">
                <div class=" container " style="margin-top: 45px;">
                    <div class="row text-center " >
                        @foreach($stats as $stat)
                        <div class="card shadow col-lg-2 col-md-3 wow fadeInUp mb-sm-30" data-wow-delay="0s " style="margin-right: 30px;    border-radius:5%">
                            <div class="de_count " style="background-size: cover; ">
                                <i class="mb-3 fa {{$stat->icon}} " style=" color: var(--secondary-color)  "></i>
                                <h3 class="">

                                    <span class=" timer " data-to="{{$stat->number}} " data-speed="3000 ">{{$stat->number}}</span>

                                </h3>
                                <h5 class="id-color ">{{$stat->name}}</h5>
                            </div>
                        </div>

                   @endforeach
                      
                    </div>
                </div>
            </section>


            <!-- testimonial-style-eight -->
            @foreach($SectionNames as $section)
           
         @if($section->imageGroups->count() || $section->partners->count() || $section->imageComments->count())
                 @if($section->imageGroups->count())
            <section class="mt-5 testimonial-style-eight">
                <div class="image-layer" 
                style="background-image: url({{asset('website_style/images/background/testimonial-bg-5.png')}});"></div>
                <div class="container" style="width: 1350px !important;">
           
                    <div class="sec-title center">
                   
                        <h2>    
                         {{$section->name}}
                        </h2>
                           </div>
                     
                 
                    <div class="three-column-carousel owl-carousel owl-theme">
                        @foreach ($section->imageGroups as $imageGroup)
                        <div class="testimonial-content">
                            <div class=" wow fadeInRight " data-wow-delay=".6s ">
                                <div class="mask rounded " style="height: 270px; ">
                                    <div class="cover rounded ">
                                        <div class="c-inner ">
                                            <h3 style="display: contents; "><i class="icofont-home "></i><span>
                                                {{$imageGroup->tittle}}
                                            </span></h3>
                                            <p style="height:110px; overflow: hidden; word-break:break-all"> {{$imageGroup->desc_image}}</p>
                                            <div class="spacer20 "></div>
                                            <a href="{{ route('internal_page_image_group.show', $imageGroup->id) }}" 
                                                class="btn-custom invert" style="width:150px"
                                                >عرض المزيد</a>
                                        </div>
                                    </div>
                                    <img src="{{ asset('storage/image_group/360/'.$imageGroup->image_name) }}" alt=" " class="img-responsive " />
                                </div>
                            </div>
                        </div>
                       
                        @endforeach

                       
                    </div>
                    </div>
                </div>
            </section>
           @endif
       

          @if( $section->imageComments->count())
            <section class="feature-style-three" style="padding-bottom: 70px !important">
                <div class="container">
                    <div class="inner-container">
                      
                       
                           <div  style="font-weight:lighter;text-align:center;">
                            <h2 style="padding-right: 20px;"> {{$section->name}}</h2>
                               </div>
                         
                            {{-- <h2> نص افتراضي نص افتراضي</h2> --}}
                     
                      
                        @foreach ($section->imageComments as $imageComment)
            

                        <div class="inner-box" style="margin-bottom: 100px !important"> 
                            <div class="row">
                                <div class="col-lg-5 col-md-12 col-sm-12 content-column">
                                    <div id="content_block_02">
                                        <div class="content-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                                            <div class="sec-title">
                                                <h2>{{$imageComment->tittle}}</h2>
                                            </div>
                                            <div class="text">
                                                <p style="word-wrap:break-word" class="text_control"> 
                                                    {{$imageComment->desc_image }}</p>
                                                
                                            </div>
                                            {{-- @if ($imageComment->desc_long) --}}
                                            {{-- <span class="more-desc" style="display:none;">{{ $imageComment->desc_long }}</span> --}}
                                            <div class="btn-box">
                                                <a  href="{{ route('internal_page_image_comment.show', $imageComment->id) }}" class="read-more  theme-btn" >اقرأ المزيد<i
                                                        class="fas fa-angle-right"></i></a></div>
                                           {{-- @endif --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-12 col-sm-12 content-column"></div>
                                <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                                    <div id="iamge_block_02">
                                        <div class="image-box">
                                            <div class="bg-layer" style="background-image: url({{asset('website_style/images/background/pattern-4.png')}});"></div>
                                            <figure class="image image-1 wow slideInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                                                <img src="{{asset('storage/image_comment/640/'.$imageComment->image_name)}}" alt="" style="height:640px"  ></figure>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach
                 
                  
                </div>
                </div>
            </section>
       
           @endif
           
      


            @if( $section->partners->count())
            <section class="feature-style-two centred" style="background-image: url({{asset('website_style/images/background/pattern-4.png')}})">
                <div class="container">
                    
                     
                       <div  style="font-weight:lighter;text-align:center;">
                        <h2 style="padding-right: 20px;">   {{$section->name}}</h2>
                        </div>
                      
                        {{-- <h2> نص افتراضي نص افتراضي</h2> --}}
                    
                    <div class="row">
                        @foreach ($section->partners as $partner)
                        <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                            <div class="feature-block-one wow flipInY animated" data-wow-delay="00ms"
                                data-wow-duration="1500ms">
                                <div class="inner-box js-tilt" style="height: 240px;">
                                    <div class="hover-content"></div>
                                    <div class="icon-box" style="border:none !important">
                                        <div class="bg-layer"
                                            style="background-image: url({{asset('website_style/images/background/feature-icon-1.png')}});
                                            right:-67px; top:-12px"
                                           >
                                           <img src="{{asset('storage/partner/640/'.$partner->image_name)}}" alt="" 
                                           style="right: 70px; top:24px; position:relative">
                                        
                                        </div>
                                       
                                    </div>
                                    <h5 style="margin-top: 75px; text-align:right"><a href="#">{{$partner->tittle}}</a></h5>
                                </div>
                            </div>
                        </div>
                    
                     @endforeach
                    </div>
                
                </div>
            </section>
              @endif
            @endif
          @endforeach

          <section class="bg-color text-light mt-3"
          style=" background-image: url({{asset('website_style/images/background/shap-3.png')}});height: 100px; ">
          <div class="container " style="background-size: cover; ">
              <div class="row " style="background-size: cover; ">

                  <h4 class="col-md-8" style="overflow: hidden; ">
                      <div class="tickerwrapper ">
                          <ul class='list'>
                              @foreach ($news as $new)
                              <li class='listitem'>
                                  <span  >
                                      <a id="nocolor" href="{{ route('internal_page_news.show', $imageComment->id) }}" >
                                       {{$new->words }}
                                       </a>
                                    </span>
                              </li>
                            @endforeach
                          </ul>
                      </div>
                  </h4>


                  <div class=" col-md-4 text-lg-center text-sm-center " style="background-size: cover; ">
                      <div class="phone-num-big " style="background-size: cover; ">
                          <button class="btn slow-down-button " data-speed="10 "
                              style="background-color: var(--secondary-color); color: #fff; margin-left: 10px;width:70px;border-radius: 15px; ">بطيء</button>
                          <button class="btn med-down-button " data-speed="20 "
                              style="background-color: var(--secondary-color); color: #fff; margin-left: 10px;width:70px;border-radius: 15px; ">متوسط</button>
                          <button class="btn speed-up-button " data-speed="60 "
                              style="background-color: var(--secondary-color); color: #fff; margin-left: 10px;width:70px;border-radius: 15px; ">سريع</button>
                      </div>

                  </div>
              </div>
          </div>
      </section>

               
                 <section class="designe-process-three service-page mt-3" style="background-image: url({{asset('website_style/images/background/testimonial-bg.png')}});">
                 <div class="container">
                     <div class="sec-title">
                        

                         <h2>هذا النص افتراضي هذا النص افتراضي<br/>هذا النص افتراضي.</h2>
                     </div>
                     <div class="inner-content" style="margin-right: 0 !important;">
                         <div class="four-item-carousel owl-carousel owl-theme owl-dots-none">
                            @foreach ($videoes as $video)
                             <div class="single-item">
                                 <div class="inner-box">
                                     <div class="video-slide">
                                    
                                         <iframe src="{{$video->link}}" 
                                         style="width: 100%; height: 100%;"></iframe>
                                         <button class="show-btn">عرض</button>
                                     <h3 class="video-tittle" style="margin-right:30px">{{$video->tittle}}</h3>
                                     <div class="text" style="margin-right:30px"> {{$video->desc_video}}.</div>
                                     </div>
                                 </div>
                             </div>
                          @endforeach
                         </div>
                     </div>
                 </div>
             </section>
    
 
        
 {{-- not ready --}}
            <section id="section-news " style=" background-color:  var(--primary-color); background-image: url({{asset('website_style/images/background/banner-bg-14.png')}}); background-size:auto !important; ">

            <div class="container ">
                <div class="row ">
                    <div class="col-lg-12 ">
                        <div class="text-center " style="float: right">
                            <span class="p-title invert " style="width: 80px; "></span><br>
                            <h2 style="color: #fff;font-weight:lighter ">هذا النص افتراضي</h2>
                            <div class="small-border "></div>
                        </div>
                    </div>
                    <div class="three-column-carousel owl-carousel owl-theme">
                    @foreach ($imageComments as $imageComment)
                    <div class=" testimonial-content" >
                        <div class="bloglist item  fadeInRight " data-wow-delay=".6s ">
                            <div class="post-content " style="border-radius: 13px;">
                          
                                <div class="post-image " >
                                    <img alt=" " src="{{asset('storage/image_comment/640/'.$imageComment->image_name)}}">
                                </div>
                                <div class="post-text " style="word-wrap: break-word; height:300px" >
                                    <h4><a href="news-single.html ">{{$imageComment->tittle}}<span></span></a>
                                    </h4>
                                    <p style="height: 150px;overflow:hidden">{{$imageComment->desc_image }}</p>
                                    {{-- @if ($imageComment->desc_long) --}}
                                    {{-- <span class="more-desc" style="display:none;">{{ $imageComment->desc }}</span> --}}
                                    <div class="btn-box">
                                        <a  href="" class="read-more  theme-btn" onclick="toggleDesc(this)"
                                        style="padding: 7px 60px 7px 20px !important;" >
                                        اقرأ المزيد<i class="fas fa-angle-right"></i></a>
                                    </div>
                                {{-- @endif --}}
                                  

                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    </div>

                </div>
                
            </div>

        </section>
      
        </div>
        <!-- content close -->

        <a href="# " id="back-to-top "></a>

        <!-- footer begin -->
        <footer>
            <div class="container ">
                <div class="row ">

                    <div class="col-lg-4 ">
                        <div class="widget ">
                            <h5>خدماتنا</h5>
                            <span class="venor-animate-border"
                            style="position: relative;
                            display: block;
                            width: 115px;
                            height: 3px;
                            background: #fff;
                            overflow: hidden;
                            opacity: .2;
                            margin-bottom: 25px;"></span>
                            <ul>
                                @foreach ($footers as $footer)
                                <li><a > {{$footer->service}}</a></li>
                              
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-5 ">
                        <div class="widget ">
                            <h5>:تواصل معنا</h5>
                            <span class="venor-animate-border"
                            style="position: relative;
                            display: block;
                            width: 115px;
                            height: 3px;
                            background: #fff;
                            overflow: hidden;
                            opacity: .2;
                            margin-bottom: 25px;"></span>
                            <address class="s1 ">
                                @foreach ($ConnectInfoFooters as $ConnectInfoFooter)
                                <span><i class="fa {{$ConnectInfoFooter->icon}} -color-secondary"></i>
                                    {{$ConnectInfoFooter->link}}</span>
                              
                               @endforeach
                            </address>
                        </div>
                    </div>

                    <div class="col-lg-3 ">
                        <div class="widget ">
                           
                                <div id="logo">
                                    <a href="index.html">
                                        <img alt="" class="logo" src="{{ asset('storage/logo/150/'.$imageName) }}" width="110px"
                                            height="50px" style="margin-top: -22px;margin-bottom: 30px;" />
                                        
                                    </a>
                                </div>
                                {{-- <span>{{$ConnectInfoFooter->link}}</span> --}}
                          
                        </div>
                    </div>

                  
                </div>
            </div>

            <div class="subfooter ">
                <div class="container ">
                    <div class="row ">
                        <div class="col-md-12 ">
                            <div class="de-flex ">
                                <div class="de-flex-col ">
                                    @foreach ($footers as $footer)
                                    &copy; {{$footer->copyright}}
                                    @endforeach
                                </div>

                                <div class="de-flex-col ">
                                    <div class="social-icons ">
                                        @foreach ($ConnectInfoFooters as $ConnectInfoFooter)
                                        <a href="{{$ConnectInfoFooter->link}}"> 
                                            <i class="fa {{$ConnectInfoFooter->icon}} -color-secondary"></i>
                                        </a>
                                        
                                        @endforeach
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </footer>
        <!-- footer close -->


    <!-- modal -->
    <div id="modal-overlay"></div>
    <div id="modalDialog" class="modal">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_vmodel"
             style="border: none;background-color: #fff;
                            ;color: #202020">
                        <span aria-hidden="true" style="margin:7px;padding-top: 10px; font-size: 27px;">&times;</span>
                        </button>
            <iframe id="modalIframe"></iframe>
            <h2 id="modalTitle"  style="margin-right:30px"></h2>
            <p id="modalDescription" style="margin-right:30px"></p>
         
        </div>

    <!-- modal -->

    </div>



    <!-- Javascript Files
    ================================================== -->
    
    <script> 
     var bootstrapUrl = '{{ asset('website_style/css/bootstrap-rtl.min.css') }}';
  var bootstrapGridUrl = '{{ asset('website_style/css/bootstrap-grid-rtl.min.css') }}';
  var bootstrapRebootUrl = '{{ asset('website_style/css/bootstrap-reboot-rtl.min.css') }}';
    </script>
    <script src="{{asset('website_style/js/jquery.min.js')}} "></script>
    <script src="{{asset('website_style/js/jpreLoader.min.js')}} "></script>
    <script src="{{asset('website_style/js/bootstrap.min.js ')}}"></script>
    <script src="{{asset('website_style/js/bootstrap.bundle.min.js')}} "></script>
    <script src="{{asset('website_style/js/wow.min.js ')}}"></script>
    <script src="{{asset('website_style/js/jquery.isotope.min.js')}} "></script>
    <script src="{{asset('website_style/js/easing.js')}} "></script>
    <script src="{{asset('website_style/js/owl.carousel.js')}} "></script>
    <script src="{{asset('website_style/js/validation.js ')}}"></script>
    <script src="{{asset('website_style/js/jquery.magnific-popup.min.js')}} "></script>
    <script src="{{asset('website_style/js/enquire.min.js')}} "></script>
    <script src="{{asset('website_style/js/jquery.stellar.min.js ')}}"></script>
    <script src="{{asset('website_style/js/jquery.plugin.js')}} "></script>
    <script src="{{asset('website_style/js/typed.js ')}}"></script>
    <script src="{{asset('website_style/js/jquery.countTo.js ')}}"></script>
    <script src="{{asset('website_style/js/jquery.countdown.js ')}}"></script>
    <script src="{{asset('website_style/js/typed.js')}} "></script>
    <script src="{{asset('website_style/js/designesia.js ')}}"></script>

    <!-- jequery plugins -->

    <script src="{{asset('website_style/additional_style/js/popper.min.js')}} "></script>
    <script src="{{asset('website_style/additional_style/js/owl.js ')}}"></script>
    <script src="{{asset('website_style/additional_style/js/wow.js')}} "></script>
    <script src="{{asset('website_style/additional_style/js/appear.js')}} "></script>
    <script src="{{asset('website_style/additional_style/js/scrollbar.js')}} "></script>
    <script src="{{asset('website_style/additional_style/js/nav-tool.js ')}}"></script>
    <script src="{{asset('website_style/additional_style/js/script.js ')}}"></script>
    <!-- RS5.0 Core JS Files -->
    <script type="text/javascript " src="{{asset('website_style/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}} "></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0 ')}}"></script>
    <!-- RS5.0 Extensions Files -->
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.video.min.js')}} "></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.layeranimation.min.js')}} "></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.navigation.min.js')}} "></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.actions.min.js')}} "></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.kenburn.min.js')}} "></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.migration.min.js ')}}"></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>






    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js "
        integrity="sha512-DkPsH9LzNzZaZjCszwKrooKwgjArJDiEjA5tTgr3YX4E6TYv93ICS8T41yFHJnnSmGpnf0Mvb5NhScYbwvhn2w=="
        crossorigin=" anonymous "></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.js "
        integrity="sha512-0xrMWUXzEAc+VY7k48pWd5YT6ig03p4KARKxs4Bqxb9atrcn2fV41fWs+YXTKb8lD2sbPAmZMjKENiyzM/Gagw=="
        crossorigin=" anonymous "></script>



    <style>
#nocolor:hover{
   color: #fff;
   /* font-size: larger;  */

}
#modal-overlay {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgb(0 0 0 / 82%);
  filter: blur(15px);
  z-index: 1090;
  display: none;
}

       #modalDialog {
    display: none;
    position: fixed;
    z-index: 1090;
    top: 110px;
    bottom: 200px;
    right: 30%; 
    width: 700px;
    height: max-content;
    background-color: #fff;
   
  }

  /* body.modal-open {
  filter: blur(5px);
} */
  #modalDialog h2 {
    color: rgb(56, 56, 56);
    font-size: 24px;
    margin-top: 20px;
    

   
  }

  #modalDialog p {
    color:  rgb(56, 56, 56);;
    font-size: 16px;
    margin-bottom: 80px;
  }

  #modalIframe {
    width: 100%;
    height: 300px;
    border: none;
  }

  #modalDialog button {
    display: block;
    margin: 20px auto;
    padding: 10px 20px;
    background-color: #fff;
    color: #000;
    font-size: 16px;
    border: none;
    cursor: pointer;
  }
       
        h2 {
            color: #3d3e3e
        }

        .de_count {


            clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);
            transition: clip-path 0.5s ease-in-out;
            animation: move-animation 2s linear infinite;
        }

        @keyframes move-animation {
            0% {
                transform: translateY(0);
            }

            50% {
                transform: translateY(-20px);
                clip-path: polygon(50% 0, 100% 50%, 50% 100%, 0 50%);
            }

            100% {
                transform: translateY(0);
                clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);
            }

        }

        .tickerwrapper {
            /* position: relative;
          top: 30px;
          left:0%; */
            /* background: #fff; */
            width: 50%;
            height: 30px;
            overflow: hidden;
            cursor: pointer;
            margin: auto;
        }

        ul.list {
            position: relative;
            display: inline-block;
            list-style: none;
            padding: 0;
            margin: 0;
        }

        ul.list.cloned {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        ul.list li {
            float: right;
            padding-left: 20px;
            color: #fff;
        }
 #closeIcon {
    top: -17px;
    float: right;
    cursor: pointer;
    font-size: 35px;
    position: absolute;
    right: 4px;}

    span.venor-animate-border::after {
    position: absolute;
    content: "";
    width: 35px;
    height: 3px;
    left: 15px;
    bottom: 0;
    border-left: 10px solid #000;
    border-right: 10px solid #000;
    -webkit-animation: animborder 2s linear infinite;
    animation: animborder 2s linear infinite;
}

    </style>

   






    <script>

        jQuery(document).ready(function () {

            // revolution slider
            jQuery("#slider-revolution ").revolution({
                sliderType: "standard ",
                sliderLayout: "fullwidth ",
                delay: 5000,
                navigation: {
                    arrows: {
                        enable: true
                    },
                    bullets: {
                        enable: true,
                        hide_onmobile: false,
                        style: "hermes ",
                        hide_onleave: true,
                        direction: "horizontal ",
                        h_align: "center ",
                        v_align: "bottom ",
                        h_offset: 20,
                        v_offset: 30,
                        space: 5,
                    },

                },
                parallax: {
                    type: "mouse ",
                    origo: "slidercenter ",
                    speed: 2000,
                    levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
                },
                responsiveLevels: [1920, 1380, 1240],
                gridwidth: [1200, 1200, 940],
                spinner: "off ",
                gridheight: 700,
                disableProgressBar: "on "
            });


        });

        const toggleBtn = document.getElementsByClassName('owl-dot');
        const group1 = document.getElementById('group1');
        const group2 = document.getElementById('group2');

        for (var i = 0; i < toggleBtn.length; i++) {
            toggleBtn[i].addEventListener('click', function () {
                if (group1.style.display !== 'none') {
                    group1.style.display = 'none';
                    group2.style.display = 'block';
                    toggleBtn.classList.add('active');
                } else {
                    group1.style.display = 'block';
                    group2.style.display = 'none';
                    toggleBtn.classList.remove('active');
                }
            })
        };
        const showButtons = document.querySelectorAll('.show-btn');
  const modalDialog = document.getElementById('modalDialog');
  const modalTitle = document.getElementById('modalTitle');
  const modalDescription = document.getElementById('modalDescription');
  const modalIframe = document.getElementById('modalIframe');

  // Add click event listener to each show button
  for (let i = 0; i < showButtons.length; i++) {
    showButtons[i].addEventListener('click', function() {
        console.log('hhh');
      const parentVideoSlide = this.parentNode;
      const src = parentVideoSlide.querySelector('iframe').src;
      const titleElement = parentVideoSlide.querySelector('.video-tittle');
      const title = titleElement ? titleElement.textContent : '';
    //   const title = parentVideoSlide.querySelector('.video-tittle').textContent;
      const description = parentVideoSlide.querySelector('.text').textContent;

      // Set the modal content
      modalTitle.textContent = title;
      modalDescription.textContent = description;
      modalIframe.src = src;

      // Show the modal dialog
     
      modalDialog.style.display = 'block';
     document.getElementById('modal-overlay').style.display='block';
    });
  }


  // Add click event listener to close the modal dialog
  //const closeButton = document.createElement('button');
  const closeButton = document.getElementById('close_vmodel');
  closeButton.addEventListener('click', function() {

    modalDialog.style.display = 'none';
    document.getElementById('modal-overlay').style.display='none';
    
    modalIframe.src = '';
   
  });
        
        var $tickerWrapper = $(".tickerwrapper ");
        var $list = $tickerWrapper.find("ul.list ");
        var $clonedList = $list.clone();
        var listWidth = 10;

        $list.find("li ").each(function (i) {
            listWidth += $(this, i).outerWidth(true);
        });

        var endPos = $tickerWrapper.width() - listWidth;

        $list.add($clonedList).css({
            "width ": listWidth + "px "
        });

        $clonedList.addClass("cloned ").appendTo($tickerWrapper);

        //TimelineMax
        var infinite = new TimelineMax({
            repeat: -1,
            paused: true
        });
        var time = 20;

        infinite
            .fromTo($list, time, {
                rotation: 0.01,
                x: 0
            }, {
                force3D: true,
                x: -listWidth,
                ease: Linear.easeNone
            }, 0)
            .fromTo($clonedList, time, {
                rotation: 0.01,
                x: listWidth
            }, {
                force3D: true,
                x: 0,
                ease: Linear.easeNone
            }, 0)
            .set($list, {
                force3D: true,
                rotation: 0.01,
                x: listWidth
            })
            .to($clonedList, time, {
                force3D: true,
                rotation: 0.01,
                x: -listWidth,
                ease: Linear.easeNone
            }, time)
            .to($list, time, {
                force3D: true,
                rotation: 0.01,
                x: 0,
                ease: Linear.easeNone
            }, time)
            .progress(1).progress(0)
            .play();



        //Pause/Play		
        $tickerWrapper.on("mouseenter ", function () {
            infinite.pause();
        }).on("mouseleave ", function () {
            infinite.play();

        });

        function updateSpeed(newTime) {
            time = newTime;
            infinite
                .fromTo($list, time, {
                    rotation: 0.01,
                    x: 0
                }, {
                    force3D: true,
                    x: -listWidth,
                    ease: Linear.easeNone
                }, 0)
                .fromTo($clonedList, time, {
                    rotation: 0.01,
                    x: listWidth
                }, {
                    force3D: true,
                    x: 0,
                    ease: Linear.easeNone
                }, 0)
                .set($list, {
                    force3D: true,
                    rotation: 0.01,
                    x: listWidth
                })
                .to($clonedList, time, {
                    force3D: true,
                    rotation: 0.01,
                    x: -listWidth,
                    ease: Linear.easeNone
                }, time)
                .to($list, time, {
                    force3D: true,
                    rotation: 0.01,
                    x: 0,
                    ease: Linear.easeNone
                }, time)
                .progress(1).progress(0)
                .play();

        }

        // تحديث سرعة الحركة عند النقر على زر تسريع الحركة
        $(".speed-up-button ").click(function () {
            updateSpeed(10);
        });

        // تحديث سرعة الحركة عند النقر على زر تبطئ الحركة
        $(".slow-down-button ").click(function () {
            updateSpeed(50);


        });

        $(".med-down-button ").click(function () {
            updateSpeed(20);


        });





    </script>



</body>

</html>