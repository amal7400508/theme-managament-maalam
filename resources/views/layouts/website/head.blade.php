<?php
    use App\Models\Logo;
    $logo = Logo::where('name','logo')->first(); // Retrieve the logo model instance
    $imageName = $logo->image_name; // Get the image name
?>

<!DOCTYPE html>
<html lang="ar">

<head>
    <meta charset="utf-8" />
    <title>{{' جمعية خيرية |'.$page_title}}</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <!-- <meta content="Priva - Insurance Company Website Template" name="description" /> -->
    <meta content="" name="keywords" />
    <meta content="" name="author" />
      <!-- Fonts -->
      <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet"/>
    
       <!-- Fonts -->
    <link rel="icon" type="image/png" sizes="36x36" href="{{ asset('storage/logo/64/'.$imageName) }}">
    <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
        <![endif]-->

    <!-- CSS Files
        
    ================================================== -->
    <!-- add Stylesheets -->
    <link id="bootstrap" href="{{ asset('website_style/css/bootstrap-rtl.min.css') }}" rel="stylesheet">
<link id="bootstrap-grid" href="{{ asset('website_style/css/bootstrap-grid-rtl.min.css') }}" rel="stylesheet">
<link id="bootstrap-reboot" href="{{ asset('website_style/css/bootstrap-reboot-rtl.min.css') }}" rel="stylesheet">
    <link href="{{asset('website_style/customized_style.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/font-awesome-all.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/flaticon.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/owl.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/jquery.fancybox.min.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/style2.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/responsive.css')}}" rel="stylesheet">


    <link href="{{asset('website_style/css/jpreloader.css')}}" rel="stylesheet" type="text/css">
    <link id="bootstrap" href="{{asset('website_style/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <link id="bootstrap-grid" href="{{asset('website_style/css/bootstrap-grid-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <link id="bootstrap-reboot" href="{{asset('website_style/css/bootstrap-reboot-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/animate.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/owl.theme.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/owl.transitions.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/jquery.countdown.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/style.css')}}" rel="stylesheet" type="text/css" />

    <!-- color scheme -->
    <link id="colors" href="{{asset('website_style/css/colors/scheme-01.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/coloring.css')}}" rel="stylesheet" type="text/css" />
    <link id="colors" href="{{asset('website_style/css/colors/scheme-01-v2.css')}}" rel="stylesheet" type="text/css" />

    <!-- RS5.0 Stylesheet -->
    <link rel="stylesheet" href="{{asset('website_style/revolution/css/settings.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('website_style/revolution/css/layers.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('website_style/revolution/css/navigation.css')}}" type="text/css">

</head>
<script>
var bootstrapUrl = '{{ asset('website_style/css/bootstrap-rtl.min.css') }}';
var bootstrapGridUrl = '{{ asset('website_style/css/bootstrap-grid-rtl.min.css') }}';
var bootstrapRebootUrl = '{{ asset('website_style/css/bootstrap-reboot-rtl.min.css') }}';
  </script>
  <script src="{{asset('website_style/js/jquery.min.js')}} "></script>
  <script src="{{asset('website_style/js/jpreLoader.min.js')}} "></script>
  <script src="{{asset('website_style/js/bootstrap.min.js ')}}"></script>
  <script src="{{asset('website_style/js/bootstrap.bundle.min.js')}} "></script>
  <script src="{{asset('website_style/js/wow.min.js ')}}"></script>
  <script src="{{asset('website_style/js/jquery.isotope.min.js')}} "></script>
  <script src="{{asset('website_style/js/easing.js')}} "></script>
  <script src="{{asset('website_style/js/owl.carousel.js')}} "></script>
  <script src="{{asset('website_style/js/validation.js ')}}"></script>
  <script src="{{asset('website_style/js/jquery.magnific-popup.min.js')}} "></script>
  <script src="{{asset('website_style/js/enquire.min.js')}} "></script>
  <script src="{{asset('website_style/js/jquery.stellar.min.js ')}}"></script>
  <script src="{{asset('website_style/js/jquery.plugin.js')}} "></script>
  <script src="{{asset('website_style/js/typed.js ')}}"></script>
  <script src="{{asset('website_style/js/jquery.countTo.js ')}}"></script>
  <script src="{{asset('website_style/js/jquery.countdown.js ')}}"></script>
  <script src="{{asset('website_style/js/typed.js')}} "></script>
  <script src="{{asset('website_style/js/designesia.js ')}}"></script>

  <!-- jequery plugins -->

  <script src="{{asset('website_style/additional_style/js/popper.min.js')}} "></script>
  <script src="{{asset('website_style/additional_style/js/owl.js ')}}"></script>
  <script src="{{asset('website_style/additional_style/js/wow.js')}} "></script>
  <script src="{{asset('website_style/additional_style/js/appear.js')}} "></script>
  <script src="{{asset('website_style/additional_style/js/scrollbar.js')}} "></script>
  <script src="{{asset('website_style/additional_style/js/nav-tool.js ')}}"></script>
  <script src="{{asset('website_style/additional_style/js/script.js ')}}"></script>
  <!-- RS5.0 Core JS Files -->
  <script type="text/javascript " src="{{asset('website_style/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}} "></script>
  <script type="text/javascript " src="{{asset('website_style/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0 ')}}"></script>
  <!-- RS5.0 Extensions Files -->
  <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.video.min.js')}} "></script>
  <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
  <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.layeranimation.min.js')}} "></script>
  <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.navigation.min.js')}} "></script>
  <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.actions.min.js')}} "></script>
  <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.kenburn.min.js')}} "></script>
  <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.migration.min.js ')}}"></script>
  <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>



