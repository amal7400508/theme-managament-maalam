<!-- BEGIN: Header-->


<?php
    use App\Models\Logo;
    $logo = Logo::where('name','logo')->first(); // Retrieve the logo model instance
    $imageName = $logo->image_name; // Get the image name
?>
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-dark shadow-lg" 
style="background: radial-gradient(#031627,#030f1a ,#010c16)">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-lg-none mr-auto"><a
                        class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                            class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="{{url('/home')}}" style="margin-right: 13px">
                        <img class="brand-text custom-logo" alt="chairty logo"
                             src="{{ asset('storage/logo/64/'.$imageName) }}" 
                             onerror="this.src='{{ url('assets/chairty_logo.png')}}'"
                             style="width:90px; margin-top: 0px;height:50px">
                    </a>

                </li>
                <li class="nav-item d-none d-lg-block nav-toggle"><a class="nav-link modern-nav-toggle pr-0"
                                                                     data-toggle="collapse"><i
                            class="toggle-icon ft-toggle-right font-medium-3 white"
                            data-ticon="ft-toggle-right"></i></a></li>
                <li class="nav-item d-lg-none"><a class="nav-link open-navbar-container" data-toggle="collapse"
                                                  data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a></li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">



                    <li class="nav-item d-none d-lg-block">
                    </li>
                    <li class="nav-item d-none d-lg-block">

                        <a class=" nav-item nav-link nav-link-expand" href="#">
                            <i class="ficon ft-maximize"></i>
                        </a>
                    </li>


                </ul>

                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-language nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ficon ft-sun" style="margin: 0"></i>
                            <span class="selected-language"></span></a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                            <a class="dropdown-item" href="{{url('style/dark')}}"><i class="la la-lightbulb-o "></i>{{__('admin.dark')}}</a>
                            <a class="dropdown-item" href="{{url('style/light')}}"><i class="la la-lightbulb-o "></i> {{__('admin.light')}}</a>
                        </div>
                    </li>

                    <li class="dropdown dropdown-language nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="flag-icon flag-icon-sa "></i>
                            <span class="selected-language"></span></a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                            <a class="dropdown-item" href="{{url('lang/en')}}"><i class="flag-icon flag-icon-gb"></i>{{__('admin.en')}} </a>
                            <a class="dropdown-item " href="{{url('lang/ar')}}"><i class="flag-icon flag-icon-sa "></i> {{__('admin.ar')}} </a>
                        </div>
                    </li>

                    <li class="dropdown dropdown-notification nav-item ">
                        <a class="nav-link nav-link-label" href="#" data-toggle="dropdown" id="notification-nav">
                            <i class="ficon ft-bell"></i><span class="font-default badge badge-pill badge-danger badge-up badge-glow" id="notifications-count"></span></a>
                        <ul class="ps__thumb-y dropdown-menu dropdown-menu-media dropdown-menu-right vertical-scroll scroll-example"
                            id="ul-notifications" style="height: 300px">
                            <li class="dropdown-menu-header" style="margin: auto 0;">
                                <h6 class="dropdown-header m-0"><span class="grey darken-2"
                                                                      style="font-size: 15px">{{__('admin.providers_notify')}}</span>
                                </h6><span class="notification-tag badge badge-danger float-right m-0"
                                           id="count-new"></span>
                            </li>
                            <li class="media-list w-100" id="notifications" style="margin:auto 0;">

                            </li>
                            <li class="dropdown-menu-footer" id="not-data" style="margin-right: auto;margin-top: 20px;"></li>
                        </ul>
                    </li>

                    <li class="dropdown dropdown-notification nav-item">
                        <a class="nav-link nav-link-label" href="#" data-toggle="dropdown" id="app-notification-nav">
                            <i class="ficon icon-bell"></i><span class="font-default badge badge-pill badge-warning badge-up badge-glow" id="app-notifications-count"></span></a>
                        <ul class="ps__thumb-y dropdown-menu dropdown-menu-media dropdown-menu-right vertical-scroll scroll-example"
                            id="ul-app-notifications" style="height: 300px">
                            <li class="dropdown-menu-header" style="margin: auto 0;">
                                <h6 class="dropdown-header m-0">
                                    <span class="grey darken-2" style="font-size: 15px">{{__('admin.apps_notify')}}</span>
                                </h6>
                                <span class="notification-tag badge badge-danger float-right m-0" id="app-ount-new"></span>
                            </li>
                            <li class="media-list w-100" id="app-notifications" style="margin:auto 0;"></li>
                            <li class="dropdown-menu-footer" id="app-not-data" style="margin-right: auto;margin-top: 20px;"></li>
                        </ul>
                    </li>

                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            <span class="mr-1 user-name text-bold-700 font-default">{{ Auth::user()->name }}</span>
                            <span class="avatar avatar-online">
                                <img id="image-sidebar"
                                     src="
                          
                                     {{asset('storage/admins/64/'.Auth::user()->image)}}
                                         "
                                     onerror="this.src='{{ url('icons/image.jpg')}}'"
                                     alt="{{ Auth::user()->name }}">
                              <i></i>
                            </span></a>
                           <div class="dropdown-menu dropdown-menu-right" dir="rtl">
                            <a class="dropdown-item" href="{{route('profile')}}"><i
                                    class="ft-user"></i>{{ __('admin.profile') }}</a>
                            <a class="dropdown-item" href="{{route('profile.edit_password', Auth::User()->id)}}"><i
                                    class="ft-edit-3"></i>{{__('admin.edit')}} {{__('admin.password')}}</a>
                            <a class="dropdown-item" href="{{env('WEBSITE_URL')}}" target="_blank"><i
                                    class="la la-globe"></i>{{ __('admin.visit_website') }}</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();"><i
                                    class="ft-power"></i>
                                {{ __('admin.Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<style>
    .img-center-64{
        width: 64px;
        height: 64px;
        object-fit: cover;
    }
</style>
<!-- END: Header-->
