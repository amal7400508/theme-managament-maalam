<script>
    let notify_count;
    let app_notify_count;
    let url = "{{url('storage/notification_movements/notify_sound1.mp3')}}";

    checkNotify();
    getAppNotify();
    getNotify();


    $(document).ready(function () {
        setInterval(checkNotify, 11000);
    });

    function checkNotify() {
        $.ajax({
            type: 'GET',
            url: "{{route('sys-notify.count')}}",
            dataType: "json",
            success: function (data) {
                if (notify_count < data.sys_notify || app_notify_count < data.app_notify) {
                    playSound(url);
                }
                if (notify_count !== data.sys_notify || app_notify_count !== data.app_notify) {
                    getNotify();
                    getAppNotify();
                    $('#notifications-count').text(data.sys_notify == 0 ? '' : data.sys_notify);
                    @can('show request')
                    $('#app-notifications-count').text(data.app_notify == 0 ? '' : data.app_notify);
                    @endcan
                }

                notify_count = data.sys_notify;
                app_notify_count = data.app_notify;
            }
        });
    }

    function playSound(url) {
        const audio = new Audio(url);
        audio.play();
    }

    function getNotify() {
        $.ajax({
            type: 'GET',
            url: "{{route('sys-notify')}}",
            dataType: "json",
            success: function (data) {
                $("#notifications").empty();
                $("#ul-notifications").attr('style', 'max-height: 300px');
                $("#not-data").attr('class', '').text('');

                data.forEach(myFunction);

                function myFunction(item, index) {
                    $("#notifications").append(item.notify);
                }

                if (!data.length) {
                    $("#ul-notifications").attr('style', '');
                    $("#not-data").attr('class', 'dropdown-menu-footer').html('<a class="dropdown-item  text-center disabled" style="color: #c9c9c9">{{__('admin.there_are_no_new_notifications')}}</a>');
                }
            }
        });
    }

    function getAppNotify() {
        @can('show request')
        $.ajax({
            type: 'GET',
            url: "{{route('app-notify')}}",
            dataType: "json",
            success: function (data) {
                $("#app-notifications").empty();
                $("#ul-app-notifications").attr('style', 'max-height: 300px');
                $("#app-not-data").attr('class', '').text('');
                data.forEach(myFunction);

                function myFunction(item, index) {
                    $("#app-notifications").append(item.notify);
                }

                if (!data.length) {
                    $("#app-notifications-count").text('');
                    $("#ul-app-notifications").attr('style', '');
                    $("#app-not-data").attr('class', 'dropdown-menu-footer').html('<a class="dropdown-item  text-center disabled" style="color: #c9c9c9">{{__('admin.there_are_no_new_notifications')}}</a>');
                }
            }
        });
        @endcan
    }

    $(document).on('click', '#route_response_at', function () {
        let notifiable_type = $(this).attr('data-type-row');
        let notifiable_id = $(this).attr('data-id-row');
        let route = "{{route('app-notify.responseAt')}}";
        $.ajax({
            type: 'POST',
            data: {
                _token: $('input[name ="_token"]').val(),
                notifiable_id: notifiable_id,
                notifiable_type: notifiable_type,
            },
            url: route,
            dataType: "json",
            success: function (data) {
                let status_append = statusAppend(data.id, data.is_new);
                td.empty().html(data.name + status_append);
                td.parent().attr('style', data.background_color_row);
            },
        });
    });

    /*$('#notification-nav').click(function () {
        window.getNotify();
    });
    $('#app-notification-nav').click(function () {
        window.getAppNotify();
    });*/


</script>
