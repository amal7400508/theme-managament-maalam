<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      
         تم  يناءه بكل
       <i class="la la-heart yellow" style="color: #d1963d;"></i> 
       بواسطة
       <img src="{{asset('assets/logo-light-icon.png')}}">
    </p>


    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
        © جميع الحقوق محفوظة
        <script>
            document.write(new Date().getFullYear());
        </script>
    </p>
</footer>
<!-- END: Footer-->
