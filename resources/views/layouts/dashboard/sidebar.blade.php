
<div class="pt-3 main-menu menu-fixed menu-accordion menu-shadow menu-light menu-bordered" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
         



                <li class="@if($page_title == __('admin.Dashboard')) active @endif">
                    <a class="menu-item"
                       @if($page_title == __('admin.Dashboard')) onclick="event.preventDefault()"
                       @endif href="{{url('/')}}"><i class="la la-home"></i>
                        <span>{{__('admin.Dashboard')}}</span></a>
                </li>
         


              



                <li class="nav-item">
                    <a href="#">
                        <i class="la  la-television"></i>
                        <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.MainSettings')}}</span>
                    </a>
                    <ul class="menu-content">

                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.FontSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.FontSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('font_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.FontSettings')}}</span>
                            </a>
                         
                        </li>


                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.LogoSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.LogoSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('logo_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.LogoSettings')}}</span>
                            </a>
                         
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.ColorSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.ColorSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('color_managment.index')}}"><i class="icon-sid la la-paper-plane">
                                           
                                       </i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.ColorSettings')}}</span>
                            </a>
                         
                        </li>

                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.SymbolsSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.SymbolsSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('symbols_managment.index')}}"><i class="icon-sid la la-paper-plane">
                                           
                                       </i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.SymbolsSettings')}}</span>
                            </a>
                         
                        </li>

                    


                        <li class="nav-item">
                            <a href="#">
                                <i class="la  la-television"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.ConectInformation')}}</span>
                            </a>
                            <ul class="menu-content">
        
                             
                              
                                <li class="nav-item">
                                    <a href="#">
                                        <i class="la  la-television"></i>
                                        <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.ConnectInfoHeader')}}</span>
                                    </a>
                                    <ul class="menu-content">
                
                                        <li class="nav-item">
                                            <a href="#">
                                                <li class="@if($page_title == __('admin.ConnectInfoHeader')) active @endif">
                                                    <a class="menu-item"
                                                       @if($page_title == __('admin.ConnectInfoHeader')) onclick="event.preventDefault()"
                                                       @endif href="{{route('connect_header_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.ConnectInfoHeader')}}</span>
                                            </a>
                                         
                                        </li>
                                        <li class="nav-item">
                                            <a href="#">
                                                <li class="@if($page_title == __('admin.OrderConnectInfoHeader')) active @endif">
                                                    <a class="menu-item"
                                                       @if($page_title == __('admin.ConnectInfoHeaderOrder')) onclick="event.preventDefault()"
                                                       @endif href="{{route('order_connect_header_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.OrderConnectInfoHeader')}}</span>
                                            </a>
                                         
                                        </li>
                                      
                                      
                                    </ul>
                                     
                                </li>
                                <li class="nav-item">
                                    <a href="#">
                                        <i class="la  la-television"></i>
                                        <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.ConnectInfoFooter')}}</span>
                                    </a>
                                    <ul class="menu-content">
                
                                        <li class="nav-item">
                                            <a href="#">
                                                <li class="@if($page_title == __('admin.ConnectInfoFooter')) active @endif">
                                                    <a class="menu-item"
                                                       @if($page_title == __('admin.ConnectInfoFooter')) onclick="event.preventDefault()"
                                                       @endif href="{{route('connect_footer_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.ConnectInfoFooter')}}</span>
                                            </a>
                                         
                                        </li>
                                        <li class="nav-item">
                                            <a href="#">
                                                <li class="@if($page_title == __('admin.OrderConnectInfoFooter')) active @endif">
                                                    <a class="menu-item"
                                                       @if($page_title == __('admin.OrderConnectInfoFooter')) onclick="event.preventDefault()"
                                                       @endif href="{{route('order_connect_footer_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.OrderConnectInfoFooter')}}</span>
                                            </a>
                                         
                                        </li>
                                      
                                      
                                    </ul>
                                     
                                </li>
        
                            </ul>
        
                        
                             
                        </li>
                    </ul>
                </li>
                <!-- END: Main Menu-->
                <li class="nav-item">
                    <a href="#">
                        <i class="la  la-television"></i>
                        <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.WebHeader')}}</span>
                    </a>
                    <ul class="menu-content">

                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.MainMenuSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.MainMenuSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('main_menu_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.MainMenuSettings')}}</span>
                            </a>
                         
                        </li>
                      
                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.SecMenuSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.SecMenuSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('sec_menu_managment.index')}}">
                                       <i class="icon-sid la la-paper-plane">

                                       </i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.SecMenuSettings')}}</span>
                                    </a>
                                </li>
                            </a>
                         
                        </li>

                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.OrderMainMenuSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.OrderMainMenuSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('order_main_menu_managment.index')}}">
                                       <i class="icon-sid la la-paper-plane">

                                       </i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.OrderMainMenuSettings')}}</span>
                                    </a>
                                </li>
                            </a>
                         
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.OrderSecMenuSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.OrderSecMenuSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('order_sec_menu_managment.index')}}">
                                       <i class="icon-sid la la-paper-plane">

                                       </i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.OrderSecMenuSettings')}}</span>
                                    </a>
                                </li>
                            </a>
                         
                        </li>
                    </ul>
                     
                </li>
                <li class="nav-item">
                    <a href="#">
                        <i class="la  la-television"></i>
                        <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.ContentWeb')}}</span>
                    </a>
                    <ul class="menu-content">

                  
                      <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.ShowAllContent')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.ShowAllContent')) onclick="event.preventDefault()"
                                       @endif href=""><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.ShowAllContent')}}</span>
                            </a>
                         
                        </li> 
                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.ImageCommentSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.ImageCommentSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('image_comment_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.ImageCommentSettings')}}</span>
                            </a>
                         
                        </li>
                      
                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.ImageGroupSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.ImageGroupSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('image_group_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.ImageGroupSettings')}}</span>
                            </a>
                         
                        </li>
                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.PartnersSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.PartnersSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('partner_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.PartnersSettings')}} (صور مصغرة)</span>
                            </a>
                         
                        </li>

                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.StaticsSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.StaticsSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('stat_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.StaticsSettings')}}</span>
                            </a>
                         
                        </li>
                  

                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.NewsSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.NewsSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('news_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.NewsSettings')}}</span>
                            </a>
                         
                        </li>
                      
                    
                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.VideoSettings')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.VideoSettings')) onclick="event.preventDefault()"
                                       @endif href="{{route('video_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.VideoSettings')}}</span>
                            </a>
                         
                        </li>

                       
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="#">
                        <i class="la  la-television"></i>
                        <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.PagesMangament')}}</span>
                    </a>
                    <ul class="menu-content">

                     
                      
                        <li class="nav-item">
                            <a href="#">
                                <i class="la  la-television"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.InternalPages')}}</span>
                            </a>
                            <ul class="menu-content">
        
                                <li class="nav-item">
                                    <a href="#">
                                        <li class="@if($page_title == __('admin.SectionName')) active @endif">
                                            <a class="menu-item"
                                               @if($page_title == __('admin.SectionName')) onclick="event.preventDefault()"
                                               @endif href="{{route('section_name_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                        <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.SectionName')}}</span>
                                    </a>
                                 
                                </li>
                              
                              
                            </ul>
                             
                        </li>

                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.SectionName')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.SectionName')) onclick="event.preventDefault()"
                                       @endif href="{{route('section_name_managment.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.SectionName')}}</span>
                            </a>
                         
                        </li>
                    </ul>

                
                     
                </li>

                <li class="nav-item">
                    <a href="#">
                        <i class="la  la-television"></i>
                        <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.LayoutsMangament')}}</span>
                    </a>
                    <ul class="menu-content">

                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.Layouts')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.Layouts')) onclick="event.preventDefault()"
                                       @endif href="{{route('layouts.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.Layouts')}}</span>
                            </a>
                         
                        </li>
                      
                      
                    </ul>
                     
                </li>
              



                <li class="nav-item">
                    <a href="#">
                        <i class="la  la-television"></i>
                        <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.WebFooter')}}</span>
                    </a>
                    <ul class="menu-content">

                        <li class="nav-item">
                            <a href="#">
                                <li class="@if($page_title == __('admin.WebFooter')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.WebFooter')) onclick="event.preventDefault()"
                                       @endif href="{{route('footer_management.index')}}"><i class="icon-sid la la-paper-plane"></i>
                                <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.WebFooter')}}</span>
                            </a>
                         
                        </li>
                      
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#">
                        <i class="la  la-television"></i>
                        <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.audit')}}</span>
                    </a>
                  
                </li>
                <li class="nav-item">
                    <a href="#">
                        <i class="la  la-television"></i>
                        <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.UsersManagment')}}</span>
                    </a>
                  
                </li>
        </ul>
    </div>
</div>

<!-- END: Main Menu-->
