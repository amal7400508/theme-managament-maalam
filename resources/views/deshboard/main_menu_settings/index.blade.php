@php($page_title = __('admin.MainMenuSettings'))
@extends('layouts.dashboard.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.HeaderSettings')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.MainMenuSettings')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            @if(isset($success))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong class="span">{{__('admin.successfully_done')}}!</strong>
                                <p>{{ $success }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                  
                                        <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.MainMenuSettings')}}</a>
                                        <a class="btn btn-primary" id="hideAddModal"><i class="ft-eye-off position-right"></i> {{__('admin.hide_model')}}</a>
                                 
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('font_managment.index')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div  id="addModal2" style="display: none">
                                        <div >
                                            <div>
                                                <section class="contact-form">
                                                <form class="form" enctype="multipart/form-data" id="form2" 
                                  
                                                method="POST" action="{{ route('main_menu_managment.store') }}">
                                                {{ csrf_field() }}
                                               
                                               
                                                        <div class="modal-header" style="padding-bottom: 0px;">
                                                            <h5 class="card-title"
                                                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.MainMenuSettings')}}</h5>
                                                            <a class="heading-elements-toggle"><i
                                                                    class="la la-ellipsis-v font-medium-3"></i></a>
                                                         
                                                        </div>

                                                        <div class="card-content collapse show">
                                                            <div class="card-body">
                                                                <div class="form-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12 p-0">
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label for="news"> اضف اسم الصفحة للقائمة الرئيسية <span class="danger"></span></label>
                                                                                        <input type="text" id="name"
                                                                                               class="form-control"
                                                                                               name="name"
                                                                                               required
                                                                                        >
                                                                                       
                                                        
                                                                                    </div>
                                                                                </div>
                                                                              
                                                  
                                                                                <div class="col-md-4">
                                                                                    <button type="submit" class="mt-2 btn btn-primary" id="btn-save2" style="width: 100%"><i
                                                                                            class="ft-save"></i> {{__('admin.save')}}</button>
                                                                                </div>
                                                                        </div>
                                                                   

                                                                          
                                                                       
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </form>
                                                </section>
                                            </div>
                                        </div>
                                    </div>

                                
                                    <div class="mt-4 card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                       
                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.name')}}</th>
            
                                                            <th>{{__('admin.action')}}</th>
                                                      
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $main_menu)
                                                        <tr style="{{$main_menu->background_color_row}}">
                                                            <td hidden>{{$main_menu->updated_at}}</td>
                                                            <td class="font-default">{{$main_menu->id}}</td>
                                                            <td>{{$main_menu->name}}</td>
                                                            <td>{!! $main_menu->actions !!}</td>
                                                         
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true" >
                                      <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <section class="contact-form">
                                                <form class="form" enctype="multipart/form-data" id="form">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                               
                                                        <div class="modal-header" style="padding-bottom: 0px;">
                                                            <h5 class="card-title"
                                                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.edit')}} {{__('admin.MainMenuSettings')}}</h5>
                                                            <a class="heading-elements-toggle"><i
                                                                    class="la la-ellipsis-v font-medium-3"></i></a>
                                                         
                                                        </div>

                                                        <div class="card-content collapse show">
                                                            <div class="card-body">
                                                                <div class="form-body">
                                                                    
                                                                     
                                                                <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="news"> عدل اسم الصفحة للقائمة الرئيسية <span class="danger"></span></label>
                                                                                        <input type="text" id="name_edit"
                                                                                               class="form-control"
                                                                                               name="name"
                                                                                               required>
                                                                                       
                                                        
                                                                                </div>
                                                                                </div>
                                                                               
                                                                              
                                                  
                                                                                <div class="col-md-6">
                                                                                <div class="form-group">


                                                                                    <button type="submit" class="mt-2 btn btn-primary" 
                                                                                    id="btn-save" ><i
                                                                                            class="ft-save"></i> {{__('admin.edit')}}</button>
                                                                                            </div>
                                                                                </div>
                                                                     
                                                                   </div>

                                                                   </div>
                                                                       
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </form>
                                                </section>
                                            </div>
                                        </div>
                                    </div>


@endsection

@section('script')

    @include('deshboard.main_menu_settings.js')
    <script>
     /*start message save*/
$('#messageSave').modal('show');
setTimeout(function () {
    $('#messageSave').modal('hide');
}, 3000);
/*end  message save*/
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
    </script>

@endsection

