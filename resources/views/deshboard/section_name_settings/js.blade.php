<script>

    let table_show = $('#table-show');
    let table_count = $('#table-count');

    $("select[name=table_length]").click(function () {
        let open = $(this).data("is_open");
        if (open) {
            window.location.href = $(this).val()
        }
        $(this).data("is_open", !open);
    });


    $(document).on('click', '#openAddModal', function () {
       

        document.getElementById('addModal2').style.display='block';
    

    
    });

    $(document).on('click', '#hideAddModal', function () {
        document.getElementById('addModal2').style.display='none';

    });
  
      /*start code edit*/
    let main_menu_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        
        main_menu_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{ url('deshboard/deshboard/section_name_managment') }}" + "/" + main_menu_id + "/edit";
      
       
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{ url('deshboard/deshboard/section_name_managment') }}" + "/" + main_menu_id)
            .attr("data_type", "Update");
       
            

        try {
            console.log(main_menu_id);
           let data =await responseEditOrShowData(url);
            document.getElementById('name_edit').value = data.name;
  
            $('#addModal').modal('show');
          

        
        } 
        
        catch (error) {
            return error;
        }

    });
    /*end code edit*/

    $('#form').on('submit', async function (event) {
        
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('main_menu_id', main_menu_id);

        try {
            form.reset();
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                let response_data = data.section;
console.log(response_data);
            $('#addModal').modal('hide');
            let status, tr_color_red = '';
            

            let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td>" + response_data.id + "</td>");
                let col2 = $("<td>" + response_data.name + "</td>");
                let col3 = $("<td>" + response_data.actions + "</td>");


                let this_row;
               
                    this_row = edit_row;
                    console.log(this_row);
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3);
                

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);}
        }
        catch (error) {}

   

    })

    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "section_name_managment" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
            this_row.remove();
        } catch (e) {
            this_row.removeClass('tr-color-active');
            console.log(777);
            return e;
        }
    });
    /*end code Delete ajax*/
</script>
