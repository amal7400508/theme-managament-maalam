<?php
    use App\Models\Logo;
    $logo = Logo::where('name','logo')->first(); // Retrieve the logo model instance
    $imageName = $logo->image_name; // Get the image name
?>

<!DOCTYPE html>
<html lang="ar">

<head>
    <meta charset="utf-8" />
    <title>التفاصيل</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <!-- <meta content="Priva - Insurance Company Website Template" name="description" /> -->
    <meta content="" name="keywords" />
    <meta content="" name="author" />
      <!-- Fonts -->
      <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet"/>
    
       <!-- Fonts -->
    <link rel="icon" type="image/png" sizes="36x36" href="{{ asset('storage/logo/64/'.$imageName) }}">
    <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
        <![endif]-->

    <!-- CSS Files
        
    ================================================== -->
    <!-- add Stylesheets -->
    <link id="bootstrap" href="{{ asset('website_style/css/bootstrap-rtl.min.css') }}" rel="stylesheet">
<link id="bootstrap-grid" href="{{ asset('website_style/css/bootstrap-grid-rtl.min.css') }}" rel="stylesheet">
<link id="bootstrap-reboot" href="{{ asset('website_style/css/bootstrap-reboot-rtl.min.css') }}" rel="stylesheet">
    <link href="{{asset('website_style/customized_style.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/font-awesome-all.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/flaticon.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/owl.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/jquery.fancybox.min.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/style2.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/responsive.css')}}" rel="stylesheet">


    <link href="{{asset('website_style/css/jpreloader.css')}}" rel="stylesheet" type="text/css">
    <link id="bootstrap" href="{{asset('website_style/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <link id="bootstrap-grid" href="{{asset('website_style/css/bootstrap-grid-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <link id="bootstrap-reboot" href="{{asset('website_style/css/bootstrap-reboot-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/animate.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/owl.theme.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/owl.transitions.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/jquery.countdown.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/style.css')}}" rel="stylesheet" type="text/css" />

    <!-- color scheme -->
    <link id="colors" href="{{asset('website_style/css/colors/scheme-01.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/coloring.css')}}" rel="stylesheet" type="text/css" />
    <link id="colors" href="{{asset('website_style/css/colors/scheme-01-v2.css')}}" rel="stylesheet" type="text/css" />

    <!-- RS5.0 Stylesheet -->
    <link rel="stylesheet" href="{{asset('website_style/revolution/css/settings.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('website_style/revolution/css/layers.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('website_style/revolution/css/navigation.css')}}" type="text/css">

</head>

<body class="hide-rtl">
    <div id="wrapper">
        <div id="topbar" class="text-light">
            <div class="container">
                <div class="topbar-left sm-hide">
                    <span class="topbar-widget tb-social">
                        @foreach ($ConnectInfoHeaders as $ConnectInfoHeader)
                        <a href="{{$ConnectInfoHeader->link}}"> 
                            <i class="fa {{$ConnectInfoHeader->icon}} -color-secondary"></i>
                        </a>
                        
                        @endforeach
                    </span>
                </div>
            
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
        <!-- header begin -->
        <header class="transparent">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="de-flex sm-pt10">
                            <div class="de-flex-col">
                                <!-- logo begin -->
                                <div id="logo">
                                    <a href="index.html">
                                        <img alt="" class="logo" src="{{ asset('storage/logo/64/'.$imageName) }}" width="80px"
                                            height="50px" style="margin-bottom: 30px;" />
                                        
                                    </a>
                                </div>
                                <!-- logo close -->
                            </div>
                            <div class="de-flex-col header-col-mid">
                                <!-- mainmenu begin -->
                                <ul id="mainmenu" style=" font-size: 18px;    margin-left: 75px;">
                                   
                                    @foreach ($mainMenuItems as $mainMenuItem)
                                   
                                    <li style="float: right;">
                                        <a href="index.html">{{ $mainMenuItem->name }}<span></span></a>
                                       
                                        <ul>
                                            @foreach ($mainMenuItem->secMenus as $secondaryMenuItem)
                                           
                                            <li style="float: right;">
                                            <a href="index.html" style="text-align: right !important;">
                                                {{$secondaryMenuItem->name}}</a> </li>
                                            @endforeach
                                      
                                        </ul>
                                    

                                      
                                    </li>
                                    @endforeach
                                  
                                    <li style="float: right;"></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header close -->
         <!-- page-title -->
    <section class="page-title" style="background-image: url({{asset('website_style/images/background/layer-image-2.png')}});
    background-color:var(--primary-color); padding: 150px 0px 150px 0px !important;">
        <div class="anim-icons">
            <div class="icon icon-1">
                <img src="{{asset('website_style/images/icons/anim-icon-17.png')}}" alt=""></div>
            <div class="icon icon-2 rotate-me"><img src="{{asset('website_style/images/icons/anim-icon-19.png')}}" alt=""></div>
            <div class="icon icon-3 rotate-me"><img src="{{asset('website_style/images/icons/anim-icon-8.png')}}" alt=""></div>
            <div class="icon icon-4"></div>
        </div>
        <div class="container">
            <div class="content-box clearfix">
                <div class="title-box pull-right">
                    <h1>الأخبار</h1>
                    <p>يمكنك التصفح وقراءة الخبر بإكمله </p>
                </div>
                <ul class="bread-crumb pull-left" style="direction: ltr">
                    <li style="font-weight: bolder; color:var(--secondary-color);" ></li>
                   
                       <li>  <a href="{{route('website.index')}}">الصفحة الرئيسية</a></li>
                </ul>
            </div>
        </div>
    </section>



       <!-- blog-classic -->
       <section class="sidebar-page-container" style="padding: 50px !important">
        <div class="container">
            <div class="row">
                <div class="post-date mb-2 " style="margin-right: -130px">
                    <i class="fas fa-calendar-alt"></i>
                    {{$data->created_at}}</div>

                <div class="col-lg-1"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 sidebar-side" style="margin-top:4px">
                    <div class="lower-content">
                        <div class="upper-box ">
                            <div class="text" style="word-break: break-all">
                            {{$data->name}}    
                              </div>
                        </div>
                        
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- blog-classic end -->

    <!-- page-title end -->

          <!-- footer begin -->
          <footer>
            <div class="container ">
                <div class="row ">

                    <div class="col-lg-4 ">
                        <div class="widget ">
                            <h5>خدماتنا</h5>
                            <span class="venor-animate-border"
                            style="position: relative;
                            display: block;
                            width: 115px;
                            height: 3px;
                            background: #fff;
                            overflow: hidden;
                            opacity: .2;
                            margin-bottom: 25px;"></span>
                            <ul>
                                @foreach ($footers as $footer)
                                <li><a > {{$footer->service}}</a></li>
                              
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-5 ">
                        <div class="widget ">
                            <h5>:تواصل معنا</h5>
                            <span class="venor-animate-border"
                            style="position: relative;
                            display: block;
                            width: 115px;
                            height: 3px;
                            background: #fff;
                            overflow: hidden;
                            opacity: .2;
                            margin-bottom: 25px;"></span>
                            <address class="s1 ">
                                @foreach ($ConnectInfoFooters as $ConnectInfoFooter)
                                <span><i class="fa {{$ConnectInfoFooter->icon}} -color-secondary"></i>
                                    {{$ConnectInfoFooter->link}}</span>
                              
                               @endforeach
                            </address>
                        </div>
                    </div>

                    <div class="col-lg-3 ">
                        <div class="widget ">
                           
                                <div id="logo">
                                    <a href="index.html">
                                        <img alt="" class="logo" src="{{ asset('storage/logo/150/'.$imageName) }}" width="110px"
                                            height="50px" style="margin-top: -22px;margin-bottom: 30px;" />
                                        
                                    </a>
                                </div>
                                {{-- <span>{{$ConnectInfoFooter->link}}</span> --}}
                          
                        </div>
                    </div>

                  
                </div>
            </div>

            <div class="subfooter ">
                <div class="container ">
                    <div class="row ">
                        <div class="col-md-12 ">
                            <div class="de-flex ">
                                <div class="de-flex-col ">
                                    @foreach ($footers as $footer)
                                    &copy; {{$footer->copyright}}
                                    @endforeach
                                </div>

                                <div class="de-flex-col ">
                                    <div class="social-icons ">
                                        @foreach ($ConnectInfoFooters as $ConnectInfoFooter)
                                        <a href="{{$ConnectInfoFooter->link}}"> 
                                            <i class="fa {{$ConnectInfoFooter->icon}} -color-secondary"></i>
                                        </a>
                                        
                                        @endforeach
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </footer>
        <!-- footer close -->


 

  



    <!-- Javascript Files
    ================================================== -->
    
    <script> 
     var bootstrapUrl = '{{ asset('website_style/css/bootstrap-rtl.min.css') }}';
  var bootstrapGridUrl = '{{ asset('website_style/css/bootstrap-grid-rtl.min.css') }}';
  var bootstrapRebootUrl = '{{ asset('website_style/css/bootstrap-reboot-rtl.min.css') }}';
    </script>
    <script src="{{asset('website_style/js/jquery.min.js')}} "></script>
    <script src="{{asset('website_style/js/jpreLoader.min.js')}} "></script>
    <script src="{{asset('website_style/js/bootstrap.min.js ')}}"></script>
    <script src="{{asset('website_style/js/bootstrap.bundle.min.js')}} "></script>
    <script src="{{asset('website_style/js/wow.min.js ')}}"></script>
    <script src="{{asset('website_style/js/jquery.isotope.min.js')}} "></script>
    <script src="{{asset('website_style/js/easing.js')}} "></script>
    <script src="{{asset('website_style/js/owl.carousel.js')}} "></script>
    <script src="{{asset('website_style/js/validation.js ')}}"></script>
    <script src="{{asset('website_style/js/jquery.magnific-popup.min.js')}} "></script>
    <script src="{{asset('website_style/js/enquire.min.js')}} "></script>
    <script src="{{asset('website_style/js/jquery.stellar.min.js ')}}"></script>
    <script src="{{asset('website_style/js/jquery.plugin.js')}} "></script>
    <script src="{{asset('website_style/js/typed.js ')}}"></script>
    <script src="{{asset('website_style/js/jquery.countTo.js ')}}"></script>
    <script src="{{asset('website_style/js/jquery.countdown.js ')}}"></script>
    <script src="{{asset('website_style/js/typed.js')}} "></script>
    <script src="{{asset('website_style/js/designesia.js ')}}"></script>

    <!-- jequery plugins -->

    <script src="{{asset('website_style/additional_style/js/popper.min.js')}} "></script>
    <script src="{{asset('website_style/additional_style/js/owl.js ')}}"></script>
    <script src="{{asset('website_style/additional_style/js/wow.js')}} "></script>
    <script src="{{asset('website_style/additional_style/js/appear.js')}} "></script>
    <script src="{{asset('website_style/additional_style/js/scrollbar.js')}} "></script>
    <script src="{{asset('website_style/additional_style/js/nav-tool.js ')}}"></script>
    <script src="{{asset('website_style/additional_style/js/script.js ')}}"></script>
    <!-- RS5.0 Core JS Files -->
    <script type="text/javascript " src="{{asset('website_style/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}} "></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0 ')}}"></script>
    <!-- RS5.0 Extensions Files -->
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.video.min.js')}} "></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.layeranimation.min.js')}} "></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.navigation.min.js')}} "></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.actions.min.js')}} "></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.kenburn.min.js')}} "></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.migration.min.js ')}}"></script>
    <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>






    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js "
        integrity="sha512-DkPsH9LzNzZaZjCszwKrooKwgjArJDiEjA5tTgr3YX4E6TYv93ICS8T41yFHJnnSmGpnf0Mvb5NhScYbwvhn2w=="
        crossorigin=" anonymous "></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.js "
        integrity="sha512-0xrMWUXzEAc+VY7k48pWd5YT6ig03p4KARKxs4Bqxb9atrcn2fV41fWs+YXTKb8lD2sbPAmZMjKENiyzM/Gagw=="
        crossorigin=" anonymous "></script>







<style>
   .page-title .content-box .bread-crumb li:before {
    position: absolute;
    content: "\f100";
    font-size: 24px;
    font-family: 'Font Awesome 5 Free';
    font-weight: 700;
    color:var(--secondary-color);
    top: -23px;
    right: 0px;
}
  .page-title .anim-icons .icon-4 {
    background-image: -webkit-linear-gradient(0deg, var(--secondary-color) 0%, var(--secondary-color) 100%);
    width: 145px;
    height: 145px;
    right: 460px;
    bottom: -30px;
    border-radius: 50%;
    box-shadow: 0 10px 30px 0px rgba(0, 0, 0, 0.1);
    -webkit-animation: zoom-fade 5s infinite linear;
    animation: zoom-fade 5s infinite linear;
}
   

.page-title .content-box .title-box h1:after {
    position: absolute;
    content: '';
    background: #eaeaea;
    width: 45px;
    height: 4px;
    right: 0px;
    bottom: 0px;
}

.page-title .content-box .title-box h1::before {
    position: absolute;
    content: '';
    background: #eaeaea;
    width: 130px;
    height: 2px;
    right: 0px;
    bottom: 0px;
}
</style>

</body>

</html>