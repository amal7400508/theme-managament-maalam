@php($page_title = __('admin.ImageGroupSettings'))
<?php
use App\Models\ImageGroup;

 ?>
@extends('layouts.dashboard.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.ContentWeb')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.ImageGroupSettings')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            @if(isset($success))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong class="span">{{__('admin.successfully_done')}}!</strong>
                                <p>{{ $success }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.ImageGroupSettings')}}</a>
                                        <a class="btn btn-primary" id="hideAddModal"><i class="ft-eye-off position-right"></i> {{__('admin.hide_model')}}</a>
                                 
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('image_group_managment.index')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="card-content collapse show" >
                                        <div class="card-body card-dashboard" >
                                            <div  id="addModal2" style="display: none">
                                            <form method="POST" action="{{ route('image_group_managment.store') }}" enctype="multipart/form-data">
                                                @csrf
                                                <p>        اختر أسم الفئة : </p>
                                                <div class="form-group">
                                                    <label></label>
                                                  
                                                   
                                               
                                                    <select type="text" id="section_name"
                                                    class="form-control"
                                                    name="section_name"
                                                    required>
                                                    <option value="">{{__('admin.select_option')}}</option>
                                                    @foreach($section_names as $section_name)
                                                        <option value="{{$section_name->id}}">{{$section_name->name}}</option>
                                                    @endforeach
                                                    </select>
                                                 
                                             
                                                 
                                                </div>
                                              <p>       اضف الصورة مع العنوان والشرح  : </p>
                                    
                                                <div class="form-group">
                                                    <label for="font_family">الصورة</label>
                                                  
                                                   
                                               
                                             
                                                    <input id="image" accept=".jpg, .jpeg, .png" type="file"
                                                           placeholder="{{__('admin.enter_image')}}"
                                                           class="form-control @error('image') is-invalid @enderror" name="image"
                                                           onchange="loadAvatar(this);" required>
                                                    <img id="avatar" style="max-width: 140px; height: auto; margin:10px;">
                                                
                                                 
                                                </div>
                                               
                                                <div class="form-group">
                                                    <label for="main_color"> العنوان</label>
                                                    <input class="form-control" id="tittle" name="tittle" type="text">
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label for="main_color"> الشرح</label>
                                                    <textarea class="form-control" id="desc_image" name="desc_image" type="text">
                                                    </textarea>
                                                </div>

                                                <div class="col-md-4" style="float: left">
                                                    <div class="form-group " style="margin-top: 28px">


                                                        <button type="submit"
                                                                class="btn btn-primary form-control" id="btn-save" style="font-size: medium"><i
                                                                class="ft-save" ></i> {{__('admin.save')}}</button>

                                                    </div>
                                                </div>
                                    
                                               
                                            </form>
                                    
                                            </div>
                                            </div>
                                          
                                        </div>
                             


                                         
                                    <div class="mt-4 card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                       
                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.name')}}</th>
                                                        <th>{{__('admin.position')}}</th>
                                                        <th>{{__('admin.PositionChange')}}</th>
                                                            <th>{{__('admin.action')}}</th>
                                                      
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $image_group)
                                                        <tr style="{{$image_group->background_color_row}}">
                                                            <td hidden>{{$image_group->updated_at}}</td>
                    
                                                            <td class="font-default">{{$image_group->id}}</td>
                                                            <td>{{$image_group->name}}</td>
                                                            <td>{{$image_group->position}}</td>
                                                            {{-- <td>{!! $image_group->getActionsAttribute(2) !!}</td> --}}
                                                            <td>{!! $image_group->postions!!}</td>
                                                            <?php
                                                               
                                                            $image_group = ImageGroup::where('section_name_id',$image_comment->id)->first(); 
                                                          
                                                             ?>
                                                         @if($image_group->status==0)
                                                            <td>{!! $image_comment->actions!!}</td>
                                                            @endif
                                                            @if($image_group->status==1)
                                                            <td>{!! $image_comment->actions2!!}</td>
                                                            @endif
                                                         
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        
                                        </div>
                                    </div>

                                </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>




    <div class="modal fade" id="ChangeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" 
    aria-hidden="true" >
                                      <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <section class="contact-form">
                                                <form class="form" enctype="multipart/form-data" id="form">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                               
                                                        <div class="modal-header" style="padding-bottom: 0px;">
                                                            <h5 class="card-title"
                                                                id="basic-layout-form" style="padding-top: 5px;"> {{__('admin.PositionChange')}}</h5>
                                                            <a class="heading-elements-toggle"><i
                                                                    class="la la-ellipsis-v font-medium-3"></i></a>
                                                         
                                                        </div>

                                                        <div class="card-content collapse show">
                                                            <div class="card-body">
                                                                <div class="form-body">
                                                                    
                                                                     
                                                                <div class="row">
                                                                               
                                                                             
                                                                                <div class="col-md-12">

                                                                                <div>
                                                                                    <span>محتوى:</span>
                                                                                    <span style="color: rgb((93, 11, 11);margin-left:10px" id="content_id"></span>
                                                                                    <br>
                                                                                    <span> التصميم:</span>
                                                                                    <br>
                                                                                    <span style="color:margin-left:10px" >الصور ضمن مجموعة تتحرك :</span>
                                                                                    <a href="{{route('layouts.index')}}#image_group" style="color: rgb(93, 11, 11);"> عرض التصميم  </a>
                                                                                    </div>

                                                                                    <div for="news" style="margin-top:15px" >
                                                                                        <span >   التصاميم الاخرى:  </span>  
                                                                                        </div>
                                                                                      <div style="margin-top:2px; " id="another_desgin">
                                                                        
                                                                                        
                                                                                        </div>


                                                                                    <div style="margin-top:15px">
                                                                                    <span>   موقعه الحالي:  </span>  
                                                                                    <span style="color: rgb(93, 11, 11)" id="curreent_postion" >   </span>  
                                                                                    <br>
                                                                                    
                                                                                        <span >   يمكنك تغيير موقعه  </span>  
                                                                                      
                                                                        
                                                                                          <span> على مستوى الصفحة بأكلمه :</span> 
                                                                                        </div>


                                                                                        </div>



                                                                                        <div class="col-md-8">
                                                                                    <div class="form-group" id="appendIt">
                                                                                     
                                                                                       
                                                                                       
                                                                                     
                                                                                        {{-- <select type="text" id="section_position" class="form-control" name="section_position"required>
                                                                                        <option value="">{{__('admin.select_option')}}</option>
                                                                                        @for($i =1; $i<=$all_conut; $i++)
                                                                                        
                                                                                     
                                                                                        @if($image_group->position==$i)
                                                                                        @continue
                                                                                    
                                                                                    
                                                                                            
                                                                                        @else
                                                                                            
                                                                                       
                                                                                            <option value="{{$i}}">{{ $image_group->position }}</option>
                                                                                            @endif
                                                                                          @endfor
                                                                                        
                                                                                        </select> --}}
                                                                                       
                                                        
                                                                                </div>
                                                                                </div>
                                                  
                                                                                <div class="col-md-4" style="margin-top: 10px">
                                                                                <div class="form-group">


                                                                                    <button type="submit" class=" btn btn-primary"  style="margin-top: 10px"
                                                                                    id="btn-save" >{{__('admin.PositionChange')}}</button>
                                                                             </div>
                                                                             </div>
                                                                              <div class="col-md-12 mt-2 mb-2">
                                                                                  <span style="color: rgb(93, 11, 11)" >*أرقام المواقع هي مساوية لعدد جميع محتوى على مستوى الموقع بأكلمه, عند اختيار رقم الموقع  لمحتوى معين, لن يظهر ضمن الاختيارات للمحتوى الاخر.</span>
                                                                              </div>
                                                                                
                                                                     
                                                                   </div>

                                                                   </div>
                                                                       
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </form>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
@endsection
@section('script')
@include('deshboard.image_group_settings.js')
    <script>
/*start message save*/
$('#messageSave').modal('show');
setTimeout(function () {
    $('#messageSave').modal('hide');
}, 3000);
/*end  message save*/
</script>



@endsection

