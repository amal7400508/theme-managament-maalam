<script>

    let table_show = $('#table-show');
    let table_count = $('#table-count');

    $("select[name=table_length]").click(function () {
        let open = $(this).data("is_open");
        if (open) {
            window.location.href = $(this).val()
        }
        $(this).data("is_open", !open);
    });


    $(document).on('click', '#openAddModal', function () {
       

        document.getElementById('addModal').style.display='block';

    
    });

    $(document).on('click', '#hideAddModal', function () {
        document.getElementById('addModal').style.display='none';

    });
  

</script>
