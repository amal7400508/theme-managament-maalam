@php($page_title = __('admin.Layouts'))
@include('layouts.dashboard.nav')
@include('layouts.dashboard.head')
<?php
    use App\Models\Logo;
    $logo = Logo::where('name','logo')->first(); // Retrieve the logo model instance
    $imageName = $logo->image_name; // Get the image name
?>

<!DOCTYPE html>
<html lang="ar">

<head>

    <meta charset="utf-8" />
    <title>{{' جمعية خيرية |'.$page_title}}</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <!-- <meta content="Priva - Insurance Company Website Template" name="description" /> -->
    <meta content="" name="keywords" />
    <meta content="" name="author" />
      <!-- Fonts -->
      <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet"/>
    
       <!-- Fonts -->
    <link rel="icon" type="image/png" sizes="36x36" href="{{ asset('storage/logo/64/'.$imageName) }}">
    <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
        <![endif]-->

    <!-- CSS Files
        
    ================================================== -->
    <!-- add Stylesheets -->

    
    <link id="bootstrap" href="{{ asset('website_style/css/bootstrap-rtl.min.css') }}" rel="stylesheet">
<link id="bootstrap-grid" href="{{ asset('website_style/css/bootstrap-grid-rtl.min.css') }}" rel="stylesheet">
<link id="bootstrap-reboot" href="{{ asset('website_style/css/bootstrap-reboot-rtl.min.css') }}" rel="stylesheet">
    

<link href="{{asset('website_style/customized_style.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/font-awesome-all.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/flaticon.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/owl.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/jquery.fancybox.min.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/style2.css')}}" rel="stylesheet">
    <link href="{{asset('website_style/additional_style/css/responsive.css')}}" rel="stylesheet">


    {{-- <link href="{{asset('website_style/css/jpreloader.css')}}" rel="stylesheet" type="text/css"> --}}
    <link id="bootstrap" href="{{asset('website_style/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <link id="bootstrap-grid" href="{{asset('website_style/css/bootstrap-grid-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <link id="bootstrap-reboot" href="{{asset('website_style/css/bootstrap-reboot-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/animate.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/owl.theme.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/owl.transitions.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/jquery.countdown.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/style.css')}}" rel="stylesheet" type="text/css" />

    <!-- color scheme -->
    <link id="colors" href="{{asset('website_style/css/colors/scheme-01.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('website_style/css/coloring.css')}}" rel="stylesheet" type="text/css" />
    <link id="colors" href="{{asset('website_style/css/colors/scheme-01-v2.css')}}" rel="stylesheet" type="text/css" />

    <!-- RS5.0 Stylesheet -->
    <link rel="stylesheet" href="{{asset('website_style/revolution/css/settings.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('website_style/revolution/css/layers.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('website_style/revolution/css/navigation.css')}}" type="text/css">

</head>


<body style="background-color: #fff; margin-top:150px">
<div class="row mt-5" >
 <div class="col-1"></div>

<div class="col-10" id="image_group">

<section class=" testimonial-style-eight shadow-lg" 
style=" background-color: #fff; padding-bottom: 0px !important; height:100%">

 <div class="image-layer" style="background-image: url({{asset('website_style/images/background/testimonial-bg-5.png')}}); height:100%"></div>
  
        <div class="sec-title center" >
            <h2 style="margin-bottom: 0px !important"> نص افتراضي نص افتراضي<br/> نص افتراضي</h2>
        </div>
        <div class="three-column-carousel owl-carousel owl-theme">
          
            <div class="testimonial-content">
                <div class=" wow fadeInRight " data-wow-delay=".6s ">
                    <div class="mask rounded " style="height: 270px; ">
                        <div class="cover rounded ">
                            <div class="c-inner ">
                                <h3 style="display: contents; "><i class="icofont-home "></i><span>
                                    نص افتراضي
                                </span></h3>
                                <p> نص افتراضي نص افتراضي</p>
                                <div class="spacer20 "></div>
                             
                            </div>
                        </div>
                        <img src="{{ asset('website_style/images/services/2.jpg') }}" alt=" " class="img-responsive " />
                    </div>
                </div>
            </div>
         
            <div class="testimonial-content">
                <div class=" wow fadeInRight " data-wow-delay=".6s ">
                    <div class="mask rounded " style="height: 270px; ">
                        <div class="cover rounded ">
                            <div class="c-inner ">
                                <h3 style="display: contents; "><i class="icofont-home "></i><span>
                                    نص افتراضي
                                </span></h3>
                                <p> نص افتراضي نص افتراضي</p>
                                <div class="spacer20 "></div>
                             
                            </div>
                        </div>
                        <img src="{{ asset('website_style/images/services/2.jpg') }}" alt=" " class="img-responsive " />
                    </div>
                </div>
            </div>
         
            <div class="testimonial-content">
                <div class=" wow fadeInRight " data-wow-delay=".6s ">
                    <div class="mask rounded " style="height: 270px; ">
                        <div class="cover rounded ">
                            <div class="c-inner ">
                                <h3 style="display: contents; "><i class="icofont-home "></i><span>
                                    نص افتراضي
                                </span></h3>
                                <p> نص افتراضي نص افتراضي</p>
                                <div class="spacer20 "></div>
                             
                            </div>
                        </div>
                        <img src="{{ asset('website_style/images/services/2.jpg') }}" alt=" " class="img-responsive " />
                    </div>
                </div>
            </div>
         
            <div class="testimonial-content">
                <div class=" wow fadeInRight " data-wow-delay=".6s ">
                    <div class="mask rounded " style="height: 270px; ">
                        <div class="cover rounded ">
                            <div class="c-inner ">
                                <h3 style="display: contents; "><i class="icofont-home "></i><span>
                                    نص افتراضي
                                </span></h3>
                                <p> نص افتراضي نص افتراضي</p>
                                <div class="spacer20 "></div>
                             
                            </div>
                        </div>
                        <img src="{{ asset('website_style/images/services/2.jpg') }}" alt=" " class="img-responsive " />
                    </div>
                </div>
            </div>
         
           
        </div>

        <div class="form-group mt-4 mb-4" style=" width:20%; float: left; margin-left:20px ">

            <a href="{{route('image_group_managment.store')}}"> <button type="submit" class="btn btn-primary form-control pt-2 pb-2 " 
             id="btn-save" style="font-size: medium"></i> استخدام التصميم</button></a>
 
         </div>

 </section>



</div>



 
<div class="col-1"></div>

</div>

<div class="row mt-5" >
    <div class="col-1"></div>
   
   <div class="col-10 shadow-lg" id="image_comment">
   
    
        
            <div class=" feature-style-three   "style="background-color: #fff; 
            padding-bottom:0px">
                <div class="inner-box" style="margin-bottom: 0px !important">
                    <div class="row">
                        <div class="col-lg-5 col-md-12 col-sm-12 content-column">
                            <div id="content_block_02">
                                <div class="content-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                                    <div class="sec-title">
                                        <h2>نص افتراضي</h2>
                                    </div>
                                    <div class="text">
                                        <p style="word-wrap:break-word" class="text_control"> 
                                            نص افتراضي نص افتراضي نص افتراضي
                                        </p>
                                        
                                    </div>
                                    
                                   
                                    <div class="btn-box"><a  class="read-more  theme-btn" >اقرأ المزيد<i
                                                class="fas fa-angle-right"></i></a></div>
                                  
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-12 col-sm-12 content-column"></div>
                        <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                            <div id="iamge_block_02">
                                <div class="image-box">
                                    <div class="bg-layer" style="background-image: url({{asset('website_style/images/background/pattern-4.png')}});"></div>
                                    <figure class="image image-1 wow slideInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                                        <img src="{{asset('website_style/images/background/dashbord-1.jpg')}}" alt="" style="height:640px"  ></figure>
                                   
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group mt-5 mb-5" style=" width:20%; float: left; margin-left:20px ">

                       <a href="{{route('image_comment_managment.store')}}"> <button type="submit" class="btn btn-primary form-control pt-2 pb-2 " 
                        id="btn-save" style="font-size: medium"></i> استخدام التصميم</button></a>
            
                    </div>
            
                </div>
     
             
               
           
          
     
        </div>
      
    
   
   
   
   
   
    </div>
   
   
   
    
   <div class="col-1"></div>
   
   </div>


   <div class="row mt-5 mb-5" >
    <div class="col-1"></div>
   
   <div class="col-10 shadow-lg" id="image_thumb">
              <!-- feature-style-two -->
              <section class="feature-style-two centred" style="background-image: url({{asset('website_style/images/background/pattern-4.png')}})">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                            <div class="feature-block-one wow flipInY animated" data-wow-delay="00ms"
                                data-wow-duration="1500ms">
                                <div class="inner-box js-tilt" style="height: 220px;">
                                    <div class="hover-content"></div>
                                    <div class="icon-box">
                                        <div class="bg-layer"
                                            style="background-image: url({{asset('website_style/images/background/feature-icon-1.png')}});"></div>
                                        <i class="flaticon-smartphone"></i>
                                    </div>
                                    <h5><a href="#">Fully Responsive</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                            <div class="feature-block-one wow flipInY animated" data-wow-delay="00ms"
                                data-wow-duration="1500ms">
                                <div class="inner-box js-tilt" style="height: 220px;">
                                    <div class="hover-content"></div>
                                    <div class="icon-box">
                                        <div class="bg-layer"
                                            style="background-image: url({{asset('website_style/images/background/feature-icon-1.png')}});"></div>
                                        <i class="flaticon-smartphone"></i>
                                    </div>
                                    <h5><a href="#">Fully Responsive</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                            <div class="feature-block-one wow flipInY animated" data-wow-delay="00ms"
                                data-wow-duration="1500ms">
                                <div class="inner-box js-tilt" style="height: 220px;">
                                    <div class="hover-content"></div>
                                    <div class="icon-box">
                                        <div class="bg-layer"
                                            style="background-image: url({{asset('website_style/images/background/feature-icon-1.png')}});"></div>
                                        <i class="flaticon-smartphone"></i>
                                    </div>
                                    <h5><a href="#">Fully Responsive</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                            <div class="feature-block-one wow flipInY animated" data-wow-delay="00ms"
                                data-wow-duration="1500ms">
                                <div class="inner-box js-tilt" style="height: 220px;">
                                    <div class="hover-content"></div>
                                    <div class="icon-box">
                                        <div class="bg-layer"
                                            style="background-image: url({{asset('website_style/images/background/feature-icon-1.png')}});"></div>
                                        <i class="flaticon-smartphone"></i>
                                    </div>
                                    <h5><a href="#">Fully Responsive</a></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                            <div class="feature-block-one wow flipInY animated" data-wow-delay="00ms"
                                data-wow-duration="1500ms">
                                <div class="inner-box js-tilt" style="height: 220px;">
                                    <div class="hover-content"></div>
                                    <div class="icon-box">
                                        <div class="bg-layer"
                                            style="background-image: url({{asset('website_style/images/background/feature-icon-1.png')}});"></div>
                                        <i class="flaticon-smartphone"></i>
                                    </div>
                                    <h5><a href="#">Fully Responsive</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                            <div class="feature-block-one wow flipInY animated" data-wow-delay="00ms"
                                data-wow-duration="1500ms">
                                <div class="inner-box js-tilt" style="height: 220px;">
                                    <div class="hover-content"></div>
                                    <div class="icon-box">
                                        <div class="bg-layer"
                                            style="background-image: url({{asset('website_style/images/background/feature-icon-1.png')}});"></div>
                                        <i class="flaticon-smartphone"></i>
                                    </div>
                                    <h5><a href="#">Fully Responsive</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                            <div class="feature-block-one wow flipInY animated" data-wow-delay="00ms"
                                data-wow-duration="1500ms">
                                <div class="inner-box js-tilt" style="height: 220px;">
                                    <div class="hover-content"></div>
                                    <div class="icon-box">
                                        <div class="bg-layer"
                                            style="background-image: url({{asset('website_style/images/background/feature-icon-1.png')}});"></div>
                                        <i class="flaticon-smartphone"></i>
                                    </div>
                                    <h5><a href="#">Fully Responsive</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                            <div class="feature-block-one wow flipInY animated" data-wow-delay="00ms"
                                data-wow-duration="1500ms">
                                <div class="inner-box js-tilt" style="height: 220px;">
                                    <div class="hover-content"></div>
                                    <div class="icon-box">
                                        <div class="bg-layer"
                                            style="background-image: url({{asset('website_style/images/background/feature-icon-1.png')}});"></div>
                                        <i class="flaticon-smartphone"></i>
                                    </div>
                                    <h5><a href="#">Fully Responsive</a></h5>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group mt-5 mb-5" style=" width:20%; float: left; margin-left:20px ">

                        <a href="{{route('partner_managment.store')}}"> <button type="submit" class="btn btn-primary form-control pt-2 pb-2 " 
                         id="btn-save" style="font-size: medium"></i> استخدام التصميم</button></a>
             
                     </div>
                </div>
            </section>
            <!-- feature-style-two end -->
          
   
        </div>
        <div class="col-1"></div>
     
   </div>
@include('layouts.dashboard.footer')




 <script>
    var bootstrapUrl = '{{ asset('website_style/css/bootstrap-rtl.min.css') }}';
    var bootstrapGridUrl = '{{ asset('website_style/css/bootstrap-grid-rtl.min.css') }}';
    var bootstrapRebootUrl = '{{ asset('website_style/css/bootstrap-reboot-rtl.min.css') }}';
      </script>
      <script src="{{asset('website_style/js/jquery.min.js')}} "></script>
      <script src="{{asset('website_style/js/jpreLoader.min.js')}} "></script>
      <script src="{{asset('website_style/js/bootstrap.min.js ')}}"></script>
      <script src="{{asset('website_style/js/bootstrap.bundle.min.js')}} "></script>
      <script src="{{asset('website_style/js/wow.min.js ')}}"></script>
      <script src="{{asset('website_style/js/jquery.isotope.min.js')}} "></script>
      <script src="{{asset('website_style/js/easing.js')}} "></script>
      <script src="{{asset('website_style/js/owl.carousel.js')}} "></script>
      <script src="{{asset('website_style/js/validation.js ')}}"></script>
      <script src="{{asset('website_style/js/jquery.magnific-popup.min.js')}} "></script>
      <script src="{{asset('website_style/js/enquire.min.js')}} "></script>
      <script src="{{asset('website_style/js/jquery.stellar.min.js ')}}"></script>
      <script src="{{asset('website_style/js/jquery.plugin.js')}} "></script>
      <script src="{{asset('website_style/js/typed.js ')}}"></script>
      <script src="{{asset('website_style/js/jquery.countTo.js ')}}"></script>
      <script src="{{asset('website_style/js/jquery.countdown.js ')}}"></script>
      <script src="{{asset('website_style/js/typed.js')}} "></script>
      <script src="{{asset('website_style/js/designesia.js ')}}"></script>
    
      <!-- jequery plugins -->
    
      <script src="{{asset('website_style/additional_style/js/popper.min.js')}} "></script>
      <script src="{{asset('website_style/additional_style/js/owl.js ')}}"></script>
      <script src="{{asset('website_style/additional_style/js/wow.js')}} "></script>
      <script src="{{asset('website_style/additional_style/js/appear.js')}} "></script>
      <script src="{{asset('website_style/additional_style/js/scrollbar.js')}} "></script>
      <script src="{{asset('website_style/additional_style/js/nav-tool.js ')}}"></script>
      <script src="{{asset('website_style/additional_style/js/script.js ')}}"></script>
      <!-- RS5.0 Core JS Files -->
      <script type="text/javascript " src="{{asset('website_style/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}} "></script>
      <script type="text/javascript " src="{{asset('website_style/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0 ')}}"></script>
      <!-- RS5.0 Extensions Files -->
      <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.video.min.js')}} "></script>
      <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
      <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.layeranimation.min.js')}} "></script>
      <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.navigation.min.js')}} "></script>
      <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.actions.min.js')}} "></script>
      <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.kenburn.min.js')}} "></script>
      <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.migration.min.js ')}}"></script>
      <script type="text/javascript " src="{{asset('website_style/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    
    
    
    

<style>
.nav-item .d-none .d-lg-block{
visibility: hidden !important;
}

</style>




    </body>
<!-- END: Body-->

</html>










                     





