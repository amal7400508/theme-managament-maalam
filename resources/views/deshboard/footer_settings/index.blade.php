<?php
    use App\Models\Logo;
    $logo = Logo::where('name','logo')->first(); // Retrieve the logo model instance
    $imageName = $logo->image_name; // Get the image name
?>


@php($page_title = __('admin.WebFooter'))
@extends('layouts.dashboard.main')

@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.WebFooter')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.WebFooter')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            @if(isset($success))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong class="span">{{__('admin.successfully_done')}}!</strong>
                                <p>{{ $success }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
            <div class="row">

                <div class="col-md-12">
                    <section>
                    
                        <div class="row card" style="padding: 35px; background-color: var(--primary-color);">

                            <div class="col-12 " >
                 
                        <footer>
                            <div class="container ">
                                <div class="row ">
                
                                    <div class="col-lg-4 ">
                                        <div class="widget ">
                                            <h5>خدماتنا</h5>
                                            <span class="venor-animate-border"
                                            style="position: relative;
                                            display: block;
                                            width: 115px;
                                            height: 3px;
                                            background: #fff;
                                            overflow: hidden;
                                            opacity: .2;
                                            margin-bottom: 25px;"></span>

                                            <ul style="margin-right: -50px; list-style:none">
                                                @foreach ($footers as $footer)
                                <li><a > {{$footer->service}}</a></li>
                              
                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                
                                    <div class="col-lg-5 ">
                                        <div class="widget ">
                                            <h5>:تواصل معنا</h5>
                                            <span class="venor-animate-border"
                                            style="position: relative;
                                            display: block;
                                            width: 115px;
                                            height: 3px;
                                            background: #fff;
                                            overflow: hidden;
                                            opacity: .2;
                                            margin-bottom: 25px;"></span>

                                               <ul style="margin-right: -50px; list-style:none">
                                                @foreach ($ConnectInfoFooters as $ConnectInfoFooter)
                                               <a href="{{$ConnectInfoFooter->link}}" style="display: block;
                                                margin-top: 6px;">
                                                
                                                 <li class="fa {{$ConnectInfoFooter->icon}}"
                                                style="font-size:large;">
                                        
                                                   
                                                </li>
                                                {{$ConnectInfoFooter->link}}
                                             <a>
                                                @endforeach
                                            </ul>


                                        </div>
                                    </div>
                
                                    <div class="col-lg-3 ">
                                        <div class="widget ">
                                           
                                                <div id="logo">
                                                    <a href="index.html">
                                                        <img alt="" class="logo" src="{{ asset('storage/logo/150/'.$imageName) }}" width="110px"
                                                            height="100px" style="margin-top: -2px;margin-bottom: 30px;" />
                                                        
                                                    </a>
                                                </div>
                                                <span>{{$ConnectInfoFooter->link}}</span>
                                          
                                        </div>
                                    </div>
                
                                  
                                </div>
                            </div>
                
                            <div class="subfooter mt- m-2">
                                <div class="container ">
                                    <div class="row ">
                                        <div class="col-md-12 ">
                                            <div class="de-flex " style="display: flex; justify-content: space-between;">
                                                <div class="de-flex-col ">
                                                    @foreach ($footers as $footer)
                                                   {{$footer->copyright}}
                                                    @endforeach
                                                </div>
                
                                                <div class="de-flex-col ">
                                                    <div class="social-icons ">
                                                        @foreach ($ConnectInfoFooters as $ConnectInfoFooter)
                                                        <a href="{{$ConnectInfoFooter->link}}"> 
                                                            <i class="fa {{$ConnectInfoFooter->icon}} -color-secondary"></i>
                                                        </a>
                                                        
                                                        @endforeach
                                                     
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                
                        </footer>
                            </div>
                       <div class="row" style="height: 53px">
                        <div class="col-4"></div>
                        <div class="col-4"></div>
                        <div class="col-4">
                            <button type="submit" class="mt-2 btn btn-primary" id="ChangeContent" style="width: 100%; float:left">
                        تغيير المحتوى</button>
                        </div>
                </div>
                            </div>
                            <div class="col-12"  style="display:none" id="TableContent">
                                <div class="card">
                                    <div class="card-header">
                                  
                                      
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('footer_management.index')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>

                                    </div>
                               

                                
                                    <div class="mt-4 card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                       
                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.services')}}</th>
                                                        
                                                        <th>{{__('admin.copyrights')}}</th>
                                                        <th>{{__('admin.LogoSettings')}}</th>
                                                        <th>{{__('admin.ConnectInfoFooter')}}</th>
                                                            <th>{{__('admin.action')}}</th>
                                                      
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $footer)
                                                        <tr style="{{$footer->background_color_row}}">
                                                            <td hidden>{{$footer->updated_at}}</td>
                                                            <td class="font-default">{{$footer->id}}</td>
                                                            <td>{{$footer->service}}</td>
                                                            <td>{{$footer->copyright}}</td>
                                                            <td>  <a  class="mt-2 btn btn-primary" href="{{route('logo_managment.index')}}"
                                                                style="width: 100%;">
                                                                تغيير الشعار</a></td>
                                                                <td>  <a  class="mt-2 btn btn-primary" href="{{route(' connect_header_managment.index')}}"
                                                                    style="width: 100%;">
                                                                    تغيير معلومات التواصل</a></td>
                                                            <td>{!! $footer->actions !!}</td>
                                                         
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true" >
                                      <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <section class="contact-form">
                                                <form class="form" enctype="multipart/form-data" id="form">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                               
                                                        <div class="modal-header" style="padding-bottom: 0px;">
                                                            <h5 class="card-title"
                                                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.edit')}} {{__('admin.MainMenuSettings')}}</h5>
                                                            <a class="heading-elements-toggle"><i
                                                                    class="la la-ellipsis-v font-medium-3"></i></a>
                                                         
                                                        </div>

                                                        <div class="card-content collapse show">
                                                            <div class="card-body">
                                                                <div class="form-body">
                                                                    
                                                                     
                                                                <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="news"> عدل اسم الصفحة للقائمة الرئيسية <span class="danger"></span></label>
                                                                                        <input type="text" id="name_edit"
                                                                                               class="form-control"
                                                                                               name="name"
                                                                                               required>
                                                                                       
                                                        
                                                                                </div>
                                                                                </div>
                                                                               
                                                                              
                                                  
                                                                                <div class="col-md-6">
                                                                                <div class="form-group">


                                                                                    <button type="submit" class="mt-2 btn btn-primary" 
                                                                                    id="btn-save" ><i
                                                                                            class="ft-save"></i> {{__('admin.edit')}}</button>
                                                                                            </div>
                                                                                </div>
                                                                     
                                                                   </div>

                                                                   </div>
                                                                       
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </form>
                                                </section>
                                            </div>
                                        </div>
                                    </div>


@endsection
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
      <!-- nesscery color scheme -->
      {{-- <link id="colors" href="{{asset('website_style/css/colors/scheme-01.css')}}" rel="stylesheet" type="text/css" /> --}}
 
@section('script')

    @include('deshboard.footer_settings.js')
    <script>
     /*start message save*/
$('#messageSave').modal('show');
setTimeout(function () {
    $('#messageSave').modal('hide');
}, 3000);
/*end  message save*/
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } });
    </script>

@endsection

