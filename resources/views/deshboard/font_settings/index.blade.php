@php($page_title = __('admin.FontSettings'))
@extends('layouts.dashboard.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.MainSettings')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.FontSettings')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            @if(isset($success))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong class="span">{{__('admin.successfully_done')}}!</strong>
                                <p>{{ $success }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                    
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('font_managment.index')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="card-content collapse show" >
                                        <div class="card-body card-dashboard" >
                                            <form method="POST" action="{{ route('font_managment.store') }}">
                                                @csrf
                                                <p>   اختر خط لعناوين الموقع: </p>
                                    
                                    
                                                <div class="form-group">
                                                    <label for="font_family">Font Family:</label>
                                                    <select class="form-control" id="font_family_title" name="font_family_title">
                                                        <option value="{{ $title_font }}">{{ $title_font }}</option>
                                                        <option value="'Nunito', sans-serif">Nunito</option>
                                                        <option value="'Roboto', sans-serif">Roboto</option>
                                                        <option value="'Open Sans', sans-serif">Open Sans</option>
                                                        <option value="Arial">Arial</option>
                                                        <option value="Times New Roman">Times New Roman</option>
                                                        <option value="Helvetica">Helvetica</option>
                                                        <option value="Georgia">Georgia</option>
                                                        <option value="Verdana">Verdana</option>
                                                        <option value="Courier New">Courier New</option>
                                                        <option value="Impact">Impact</option>
                                                        <option value="Comic Sans MS">Comic Sans MS</option>
                                                        <option value="Tahoma">Tahoma</option>
                                                        <option value="Palatino">Palatino</option>
                                                        <option value="monospace">Monospace</option>
                                                    </select>
                                                </div>
                                                <p> (Paragrahp)  اختر خط لفقرات الموقع: </p>
                                              
                                                <div class="form-group">
                                                    <label for="font_family">Font Family:</label>
                                                    <select class="form-control" id="font_family_body" name="font_family_body">
                                                        <option value="{{ $body_font }}">{{ $body_font }}</option>
                                                        <option value="'Nunito', sans-serif">Nunito</option>
                                                        <option value="'Roboto', sans-serif">Roboto</option>
                                                        <option value="'Open Sans', sans-serif">Open Sans</option>
                                                        <option value="Arial">Arial</option>
                                                        <option value="Times New Roman">Times New Roman</option>
                                                        <option value="Helvetica">Helvetica</option>
                                                        <option value="Georgia">Georgia</option>
                                                        <option value="Verdana">Verdana</option>
                                                        <option value="Courier New">Courier New</option>
                                                        <option value="Impact">Impact</option>
                                                        <option value="Comic Sans MS">Comic Sans MS</option>
                                                        <option value="Tahoma">Tahoma</option>
                                                        <option value="Palatino">Palatino</option>
                                                        <option value="monospace">Monospace</option>
                                                    </select>
                                                </div>
                                                <p>  اختر خط للقوائم: </p>
                                              
                                                <div class="form-group">
                                                    <label for="font_family">Font Family:</label>
                                                    <select class="form-control" id="font_family_menu" name="font_family_menu">
                                                        <option value="{{ $menu_font }}">{{ $menu_font }}</option>
                                                        <option value="'Nunito', sans-serif">Nunito</option>
                                                        <option value="'Roboto', sans-serif">Roboto</option>
                                                        <option value="'Open Sans', sans-serif">Open Sans</option>
                                                        <option value="Arial">Arial</option>
                                                        <option value="Times New Roman">Times New Roman</option>
                                                        <option value="Helvetica">Helvetica</option>
                                                        <option value="Georgia">Georgia</option>
                                                        <option value="Verdana">Verdana</option>
                                                        <option value="Courier New">Courier New</option>
                                                        <option value="Impact">Impact</option>
                                                        <option value="Comic Sans MS">Comic Sans MS</option>
                                                        <option value="Tahoma">Tahoma</option>
                                                        <option value="Palatino">Palatino</option>
                                                        <option value="monospace">Monospace</option>
                                                    </select>
                                                </div>

                                                
                                                <div class="col-md-4" style="float: left">
                                                    <div class="form-group " style="margin-top: 28px">


                                                        <button type="submit"
                                                                class="btn btn-primary form-control" id="btn-save" style="font-size: medium"><i
                                                                class="ft-save" ></i> {{__('admin.save')}}</button>

                                                    </div>
                                                </div>
                                    
                                               
                                            </form>
                                    

                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>




@endsection

@section('script')
    <script>
/*start message save*/
$('#messageSave').modal('show');
setTimeout(function () {
    $('#messageSave').modal('hide');
}, 3000);
/*end  message save*/
</script>
@endsection