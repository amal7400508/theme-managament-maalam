@php($page_title = __('admin.LogoSettings'))
@extends('layouts.dashboard.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.MainSettings')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.LogoSettings')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">

        @if(isset($success))
        <div id="messageSave" class="modal fade text-left" role="dialog">
            <div class="modal-dialog">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="alert bg-info alert-icon-left mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                            <strong class="span">{{__('admin.successfully_done')}}!</strong>
                            <p>{{ $success }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                    
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('font_managment.index')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="card-content collapse show" style="height: 400px">
                                        <div class="card-body card-dashboard" >
                                            <form method="POST" action="{{ route('logo_managment.store') }}" enctype="multipart/form-data">
                                                @csrf
                                              <p>     اختر صورة لتغيير شعار الموقع  : </p>
                                    
                                                <div class="form-group">
                                                    <label for="font_family"> الشعار</label>
                                                  
                                                   
                                                </div>
                                             
                                                    <input id="image" accept=".jpg, .jpeg, .png" type="file"
                                                           placeholder="{{__('admin.enter_image')}}"
                                                           class="form-control" name="image"
                                                            required>
                                                    <img id="avatar" style="max-width: 140px; height: auto; margin:10px;">
                                                    @error('image')
                                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                    @enderror
                                                    <br><br><br>
                          
                                               
                                                   
                                                <div class="col-md-4" style="float: left">
                                                    <div class="form-group " style="margin-top: 28px">


                                                        <button type="submit"
                                                                class="btn btn-primary form-control" id="btn-save" style="font-size: medium"><i
                                                                class="ft-save" ></i> {{__('admin.save')}}</button>

                                                    </div>
                                                </div>
                                    
                                               
                                            </form>
                                    

                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>




@endsection
@section('script')
    <script>
/*start message save*/
$('#messageSave').modal('show');
setTimeout(function () {
    $('#messageSave').modal('hide');
}, 3000);
/*end  message save*/
</script>
@endsection

