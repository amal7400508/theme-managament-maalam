<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'admin',
            'image' => 'admin_default.png',
            'email' => 'admin@admin.com',
            'style' => 'Light',
          
            
            'password' => bcrypt('123456789'),
        ]);
        // $user->attachRole('super_admin');
    }
}
