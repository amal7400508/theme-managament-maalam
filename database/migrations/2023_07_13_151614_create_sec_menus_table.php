<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   

    // Create the sec_menus table here

    public function up()
    {if (!Schema::hasTable('sec_menus')) {
        Schema::create('sec_menus', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
            $table->integer('order')->default(0);
            $table->unsignedBigInteger('main_menu_id');
            $table->foreign('main_menu_id')->references('id')->on('main_menus');
        });
    }
}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sec_menus');
    }
};
