<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_group', function (Blueprint $table) {
            $table->id();
            $table->string('image_name');
            $table->string('tittle');
            $table->text('desc_image');
            $table->string('status')->default(1);
            $table->unsignedBigInteger('section_name_id');

            $table->foreign('section_name_id')->references('id')->on('section_name');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_group');
    }
};
